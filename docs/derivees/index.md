# 1MATHS : Nombre Dérivé. Fonction Dérivée

## Taux de Variation

Dans tout ce chapitre, $f$ désigne une fonction définie sur un intervalle $I$

!!! def
    Soit $A(a ;f(a))$ et $B(b ;f(b))$ deux points de la courbe $\mc C$.  
    Le <red>taux de variation</red>, ou <red>taux d'accroissement</red>, de $f$ entre $a$ et $b$ est le nombre $T(a;b)$ défini par :  
    <center><enc>$T(a;b)=\dfrac {f(b)-f(a)}{b-a}= \dfrac {\Delta y}{\Delta x}$</enc></center>

!!! pte "Interprétation Graphique du Taux de Variation $T(a;b)$"
    Graphiquement, le taux de variation $T(a;b)$ est le coefficient directeur/la pente de la droite $(AB)$ qui passe par $A(a ;f(a))$ et $B(b ;f(b))$

<iframe id="nombrederive" src="https://www.geogebra.org/classic/zqjgp9fb?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

!!! remarque
    $T(a;b)=T(b;a)$ car $\dfrac {f(b)-f(a)}{b-a}=\dfrac {f(a)-f(b)}{a-b}$  
    Graphiquement, on retrouve que le coefficient directeur/la pente de la droite $(AB)$ est égale au coefficient directeur/la pente de la droite $(BA)$

<env>En pratique</env> on calcule le taux de variation $T(a;x)=\dfrac {f(x)-f(a)}{x-a}$ entre un point $A\coord {a}{f(a)}$ fixe, et un autre point $M \coord {x}{f(x)}$ dit **mobile**, dont on fait varier l'abscisse $x$.

<iframe id ="nombrederiveavecpointmobile" src="https://www.geogebra.org/classic/s95qnkjz?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

!!! ex
    Soit $f$ définie par $f(x) = 5x^2$.
    
    1. Déterminer $T(3 ;3+h)$ en fonction de $h$ (où $h$ désigne un nombre réel variable).
    1. Déterminer $T(0 ;0+h)$ en fonction de $h$ (où $h$ désigne un nombre réel variable).
    1. Déterminer $T(1 ;1+h)$ en fonction de $h$ (où $h$ désigne un nombre réel variable).
    1. Déterminer $T(2 ;2+h)$ en fonction de $h$ (où $h$ désigne un nombre réel variable).
    1. Déterminer $T(-2 ;-2+h)$ en fonction de $h$ (où $h$ désigne un nombre réel variable).

## Fonction Dérivable en $a$

!!! def "Fonction Dérivable en $a$"
    Soit $f$ une fonction définie sur un intervalle $I$ contenant le nombre réel $a$.  
    On dit que la fonction <red>$f$ est dérivable en $a$</red> (" si et seulement si ")  
    ssi La fonction $h \mapsto \dfrac {f (a + h ) − f (a)}{h}$ admet un réel fini $\ell$ pour limite en $0$ (lorsque $h \to 0$)  
    ssi Il existe un réel fini $\ell$ tel que $\displaystyle \lim_{h\to0} \dfrac {f(a+h) − f(a)}{h}=\ell$  
    ssi la fonction $x \mapsto \dfrac {f(x)-f(a)}{x-a}$ admet un réel fini $\ell$ pour limite en $a$ (lorsque $x\to a$)  
    ssi Il existe un réel fini $\ell$ tel que $\displaystyle \lim_{x\to a} \dfrac {f(x)-f(a)}{x-a} = \ell$ $\quad$ (Poser $x = a + h$)  

!!! remarque "CULTURE : :warning: Théorie (non toujours dérivable) vs Pratique (souvent dérivable):warning:"
    <env>En théorie</env> Une fonction $f$ n'est **pas obligatoirement/pas nécessairement** dérivable en un réel $a$.  
    Les $3$ situations distinctes suivantes, pouvant toutes se produire, correspondent à une fonction $f$ **non dérivable** en un point $a$ :

    * ou bien la limite en elle-même n'existe pas (par exemple, le taux de variation de $f$ oscille infiniment entre des valeurs, au lieu de s'approcher infiniment d'une même et unique valeur $\ell$)
    * ou bien la limite existe mais vaut $+\infty$
    * ou bien la limite existe mais vaut $-\infty$
    
    :warning: ATTENTION MONSTRES :warning: On peut même montrer qu'il existe certaines fonctions (très complexes), considérées comme des *monstres mathématiques* telles que :  

    * elles sont définies sur un intervalle $D$ 
    * mais ne sont dérivables en aucun point $a$ de $D$  

    <env>En pratique</env> Les fonctions usuelles, définies sur un intervalle $D$, sont dérivables en tous les points $a$ de $D$ (sauf éventuellement aux extrémités de $D$)

## Nombre Dérivé

!!! def "Nombre Dérivé $f'(a)$"
    Lorsque les deux limites (équivalentes) suivantes existent (une seule suffit) et est égale à un nombre réel fini, on la/le note <enc>$f'(a)$</enc>, et on l'appelle le <red>nombre dérivé de $f$ en $a$</red>.  

!!! pte
    Le nombre dérivé $f'(a)$ existe  
    $\ssi$ la fonction $f$ est dérivable en $a$  
    $\ssi$ le taux de variation de $f$, càd la fonction $x\mapsto T(a;x)=\dfrac {f(x)-f(a)}{x-a}$ admet une limite en $a$  
    $\ssi$ les deux limites (équivalentes) suivantes existent :  

    <center><enc>$\displaystyle f'(a)=\lim_{h\to 0} \dfrac{f(a+h)−f(a)}{h}$</enc> $\quad$ et $\quad$ <enc>$\displaystyle f'(a)=\lim_{x\to a} \dfrac{f(x)−f(a)}{x-a}$</enc></center>  
    Ainsi, <red>$f'(a)$ est la limite, lorsque $x\to a$, du taux de variation $T(a;x)=\dfrac {f(x)-f(a)}{x-a}$</red> (lorsqu'elle existe)  

!!! ex
    Soit $f$ définie par $f(x) = 5x^2$.

    1. D'après l'exercice précédent, on sait que $T(3; 3+h)=5h+30$.  
    En déduire la valeur du nombre dérivé $f'(3)$ (s'il existe).  
    
        ??? corr
            Pour la fonction $f$ définie par $f(x) = 5x^2$, on a vu que $T(3 ;3+h) = 5h + 30$.  
            Lorsque $h$ tend vers $0$ (comprendre lorsque $h$ *devient très proche de $0$*), alors :
            
            * le nombre $5h$ tend vers $0$ également (comprendre *devient très proche de $0$*) et donc 
            * $30 + 5h$ tend vers $30$ (comprendre *devient très proche de $30$*).
            * Pour résumer ce dernier résultat, on note: $\displaystyle \lim_{h\to0} 30 + 5h = 30$.

    1. En suivant le même mode opératoire, déterminer les valeurs des nombres dérivés suivants (s'ils existent):  

        * $f'(0)$
        * $f'(1)$
        * $f'(2)$
        * $f'(-1)$