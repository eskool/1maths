# DS Exponentielle

!!! ex
    Résoudre sur l'intervalle $\mathbb R$, chacune des équations demandées ci-dessous. En justifiant et en rédigeant.

    1. $e^{-2x} = 1$
    2. $e^{3x} = -1$
    3. $e^{4x+5} = e^{7x+2}$
    4. $e^{3x^2+2x} = e^{5x+4}$

!!! ex
    Résoudre sur l'intervalle $\mathbb R$, chacune des inéquations demandées ci-dessous. En justifiant et en rédigeant.

    1. $e^{2x} > 1$
    2. $e^{-3x} \le 1$
    3. $e^{8x+2} \le e^{4x+7}$
    4. $e^{2x^2+3x} \ge e^{7x+5}$

!!!e ex
    Soit $f$ la fonction définie sur l'intervalle $I=[-5; 2]$, par $f(x)= (5x^2+2x-1) \times e^{1,4x}$

    1. Montrer que la fonction dérivée $f'(x)$ peut s'écrire sous la forme:

    $$f'(x) = e^{1,4x}\times \left( 7x^2 +12,8x+0,6\right)$$

    2. Étudier le signe de $f'(x)$, autrement dit : Résoudre l'inéquation $f'(x)>0$

    3. En déduire le Tableau de Variation de $f$ sur $\mathbb R$

    4. Étudier les extrema locaux sur $\mathbb R$, autrement dit: Existe-t-il des extrema locaux? 
        * Si NON, pourquoi?
        * Si OUI:
            * Déterminer les extrema locaux
            * Est-ce que ce sont des minima? des maxima? pourquoi?
            * En quelles valeurs ces extrema sont-ils atteints?

    5. Faites un graphique (approximatif) de la courbe de $f$ dans un repère. Unités: 

        * Une unité sur l'axe des $x$ correspond à $1$ cm.
        * Une unité sur l'axe des $y$ correspond à $1$ cm.
