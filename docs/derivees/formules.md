# 1MATHS : Fonction Dérivée & Formulaire

### Fonction Dérivable sur un intervalle $I$

!!! def "Fonction $f$ dérivable sur un intervalle $I$"
    On dit qu'une fonction <red>$f$ est dérivable sur un intervalle $I$</red>,  
    ssi $f$ est dérivable en tout nombre réel $a$ appartenant à l'intervalle $I$,  
    ssi $f'(a)$ existe pour tout nombre $a\in I$

### Fonction Dérivée

!!! def "Fonction Dérivée"
    Lorsque la fonction $f$ est dérivable sur un intervalle $I$, la fonction $f': \, x\mapsto f'(x)$, est appelée <red>la fonction dérivée de $f$ sur $I$</red>.  
    C'est la fonction qui à chaque $x\in I$, associe le nombre dérivé $f'(x)$.

!!! remarque
    <env>En théorie</env> étant donné qu'un nombre dérivé n'existe pas obligatoirement, une fonction $f$ n'est donc pas obligatoirement dérivable (nulle part sur $I$? seulement en certains points $a$ sur $I$? partout sur $I$?), donc la fonction dérivée $f'$ n'existe pas obligatoirement (sur $I$? sur seulement un sous-ensemble de $I$? sur aucun ensemble de $I$?). Il faudrait systématiquement justifier l'existence de la fonction dérivée $f'$.  
    <env>En pratique</env> des théorèmes à venir affirmeront que les fonctions usuelles $f$ admettent obligatoirement des fonctions dérivées (définies sur toutes -ou presque- les valeurs $a$ de l'ensemble de définition de $f$).

## Formulaire : Dérivées des Fonctions usuelles

!!! pte "Fonctions dérivées des fonctions usuelles"
    | $f(x)=$ | $f'(x)=$ | $f$ est dérivable sur ... |
    |:-:|:-:|:-:|
    | $k$ (constante) | $0$ | $\m R$ |
    | $x$ | $1$ | $\m R$ |
    | $x^n$ | $nx^{n-1}$ | $I=\begin{cases} \m R \text{ , lorsque } n \text{ est entier, } n\ge 0  \\ \left]-\infty;0\right[ \text { ou bien } \left]0;+\infty\right[ \text{ , lorsque } n \text{ est entier, } n\le -1\\ \end{cases}$ |
    | $\dfrac 1x$ | $-\dfrac 1{x^2}$ | $\m R^{*}=\m R \setminus \{0\}=\left]-\infty;0\right[ \cup \left]0;+\infty\right[$ <br/> Lire "$\m R$ privé de $0$" |
    | $\sqrt x$ | $\dfrac 1{2\sqrt x}$ | $\m R^{+*} = \left]0;+\infty\right[$ <br/> En particulier, $x\mapsto \sqrt x$ n'est pas dérivable en $0$ |
    | $\sin x$ | $\cos x$ | $\m R$ |
    | $\cos x$ | $-\sin x$ | $\m R$ |
    | $\tan x$ | $\dfrac {1}{\cos^2(x)}=1+\tan^2(x)$ | $\m R \setminus \left\{\dfrac \pi{2}+k\pi  \text{ lorsque } k\in \m Z \right\}$ |

!!! exp
    1. Si $f(x) = 3$ alors <enc>$f'(x) = 0$</enc>
    1. Si $f(x) = x^2$ alors $f'(x) = 2x^{2 – 1} = 2x^1$ donc <enc>$f'(x) = 2x$</enc>
    1. Si $f(x) = x^3$ alors $f'(x) = 3x^{3-1}$ donc <enc>$f'(x)=3x^2$</enc>
    1. Si $f(x)=\dfrac {1}{x^7}=x^{-7}$ alors $f'(x)=-7x^{-7-1}=-7x^{-8}$ donc <enc>$f'(x)=\dfrac {-7}{x^8}$</enc>

!!! ex
    Déterminer les fonctions dérivées $f'$ pour les fonctions $f$ suivantes :
    1. $f(x) = 4$
    1. $f(x) = x^5$
    1. $f(x) =\dfrac {1}{x^2}$
    1. $f(x) =\dfrac {1}{x^3}$


## Formulaire : Opérations sur les dérivées

!!! pte
    | $f(x)=$ | $f'(x)=$ | $f$ est dérivable sur ... |
    |:-:|:-:|:-:|
    | $u+v$ | $u'+v'$ | Si $u$ et $v$ sont dérivables sur $I$, <br/>Alors $u+v$ est dérivable sur $I$ |
    | $u-v$ | $u'-v'$ | Si $u$ et $v$ sont dérivables sur $I$, <br/>Alors $u-v$ est dérivable sur $I$ |
    | $k\times u$ | $k\times u'$ | Si $u$ est dérivable sur $I$, et $k$ est constante<br/>Alors $k\times u$ est dérivable sur $I$ |
    | $u\times v$ | $u'v+uv'$ | Si $u$ et $v$ sont dérivables sur $I$,<br/>Alors $u\times v$ est dérivable sur $I$ |
    | $\dfrac uv$ | $\dfrac {u'v-uv'}{v^2}$ | Si $u$ et $v$ sont dérivables sur $I$, <br/>Alors $\dfrac uv$ est dérivable sur $I$,<br/>privé des $x\in I$ tels que $v(x)=0$ |
    | $\dfrac 1v$ | $\dfrac {-v'}{v^2}$ | Si $v$ est dérivable sur $I$, <br/>Alors $\dfrac 1v$ est dérivable sur $I$, <br/>privé des $x\in I$ tels que $v(x)=0$ |
    | $u^n$ | $nu'u^{n-1}$ | Si $u$ est dérivable sur $I$,<br/>Alors $u^n$ est dérivable sur<br/> $I=\begin{cases} \m R \text{ , lorsque } n \text{ est entier, } n\ge 1  \\ \m R \setminus \{ x \text{ tels que } u(x)=0 \} \text{ , lorsque } n \text{ est entier, } n\lt 1\\ \end{cases}$ |
    | $\sqrt u$ | $\dfrac {u'}{2\sqrt u}$ | Si $u$ est dérivable sur $I$,<br/>Alors $\sqrt u$ est dérivable sur<br/> $I= \m R \setminus \{ x \text{ tels que } u(x)=0 \}$ |

!!! exp
    1. Si $f(x) = –4x^2$ alors $f'(x) = –4\times (x^2)' = –4\times 2x$ donc <enc>$f'(x) = –8x$</enc>
    1. Si $f(x) =  \dfrac {x^3}{5}=\dfrac 15 \times x^3$ alors <enc>$f'(x) = \dfrac 15 \times 3 \times x^2 = x5$</enc>
    1. Si $f(x) = –3x^2 + 4x – 7$ alors <enc>$f'(x) = –6x + 4$</enc>
    1. Si $f(x) = (–2x – 5)(–2x^2 + 3x + 7)$ alors on voit que 
        
        * $f$ est de la forme $f(x) = u(x) v(x)$ où l'on a posé $\begin{cases} u(x) = –2x – 5 \\ v(x) = –2x^2 + 3x + 7 \end{cases}$
        * donc $f'(x) = u'v + uv'$ avec $\begin{cases} u(x) = –2x – 5\\ u'(x) = -2\\ v(x) = –2x^2 + 3x + 7 \\ v'(x)= –4x + 3 \end{cases}$
        * donc : $\begin{align} f'(x) &= u'v + uv' \\
                            &= –2\times (–2x^2 + 3x + 7) + (–2x − 5)\times (–4x + 3) \\
                            &= 4x^2 –6x –14 +(8x^2 –6x +20x − 15) \\
                            f'(x) &= 12x^2 + 8x –29 
                            \end{align}
                            $
    1. Si $f(x) = \dfrac {-2x+1}{3x-5}$ alors on voit que 
        
        * $f$ est de la forme $f(x) = \dfrac {u(x)}{v(x)}$ où l'on a posé $\begin{cases} u(x) = –2x +1 \\ v(x) = 3x-5 \end{cases}$
        * donc $f'(x) = \dfrac {u'v - uv'}{v^2}$ avec $\begin{cases} u(x) = –2x +1 \\ u'(x) = -2 \\ v(x) = 3x-5 \\ v'(x) = 3 \end{cases}$
        * donc : $\begin{align} f'(x) &= \dfrac {u'v - uv'}{v^2} \\
                            &= \dfrac {–2\times (3x-5) - (–2x+1)\times (3)}{(3x-5)^2} \\
                            &= \dfrac {\cancel{-6x}+10+\cancel{6x}-3}{(3x-5)^2} \\
                            &= \dfrac {7}{(3x-5)^2} \\
                            \end{align}
                            $

!!! ex
    Déterminer les (fonctions) dérivées $f'$ des fonctions $f$ suivantes :

    1. $f(x) = 5x^7 − 3x^5 + 4x^3 − 3x^2 + 17,4$
    1. $f(x) = (3x^2 − 1)(3x^2 + 5x + 1)$
    1. $f(x) = \dfrac {3x+1}{− x + 4}$
    1. $f(x) = \dfrac {2x^2-3x+5}{x^3−3x^2+x−1}$

