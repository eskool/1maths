# DS Exponentielle

!!! ex
    Résoudre sur l'intervalle $\mathbb R$, chacune des équations demandées ci-dessous. En justifiant et en rédigeant.

    1. $e^{-3x+2} = 1$
    2. $e^{2x+7} = -1$
    3. $e^{2x+4} = e^{5x-1}$
    4. $e^{3x^2+4x} = e^{-x+2}$

!!! ex
    Résoudre sur l'intervalle $\mathbb R$, chacune des inéquations demandées ci-dessous. En justifiant et en rédigeant.

    1. $e^{2x+5} > 1$
    2. $e^{-3x+1} \le 1$
    3. $e^{6x+1} \le e^{2x+6}$
    4. $e^{2x^2+4x} \ge e^{-2x+3}$

!!! ex
    Soit $f$ la fonction définie sur l'intervalle $I=[-4; 1]$, par $f(x)= (3x^2+4x-5) \times e^{1,2x}$

    1. Montrer que la fonction dérivée $f'(x)$ peut s'écrire sous la forme:

    $$f'(x) = e^{1,2x}\times \left( 3,6x^2 +10,8x-2\right)$$

    2. Étudier le signe de $f'(x)$, autrement dit : Résoudre l'inéquation $f'(x)>0$

    3. En déduire le Tableau de Variation de $f$ sur $\mathbb R$

    4. Étudier les extrema locaux sur $\mathbb R$, autrement dit: Existe-t-il des extrema locaux? 
        * Si NON, pourquoi?
        * Si OUI:
            * Déterminer les extrema locaux
            * Est-ce que ce sont des minima? des maxima? pourquoi?
            * En quelles valeurs ces extrema sont-ils atteints?

    5. Faites un graphique (approximatif) de la courbe de $f$ dans un repère. Unités: 

        * Une unité sur l'axe des $x$ correspond à $1$ cm.
        * Une unité sur l'axe des $y$ correspond à $1$ cm.
