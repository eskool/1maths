# DM Équations et Inéquations avec Exponentielle

A Rendre pour le Jour de la Rentrée : <span style="font-size:1.5em;">**Lundi 26 Avril 2021**</span>

!!! ex
    Résoudre sur l'intervalle $\mathbb R$, chacune des équations demandées ci-dessous. En justifiant et en rédigeant.

    1. $e^{x} = -2$
    2. $e^{x} = 0$
    3. $e^{3x+1} = e^{5x+3}$
    3. $e^{5x^2+x} = e^{4x+1}$

!!! ex
    Résoudre sur l'intervalle $\mathbb R$, chacune des inéquations demandées ci-dessous. En justifiant et en rédigeant.

    1. $e^{x} < -1$
    2. $e^{x} \ge 0$
    3. $e^{7x+3} \le e^{4x+5}$
    3. $e^{x^2+2x} \ge e^{4x+3}$
