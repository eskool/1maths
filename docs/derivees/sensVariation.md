# 1MATHS : Cours sur les Dérivées & Sens de Variation

## Signe de la Dérivée et Sens de Variation

!!! thm
    Soit $f$ une fonction dérivable sur un Intervalle $I$ :

    * $f'\ge0$ sur $I$ $\ssi$ $f$ est croissante sur $I$
    * $f'\le0$ sur $I$ $\ssi$ $f$ est décroissante sur $I$
    * $f'=0$ sur $I$   $\ssi$ $f$ est constante sur $I$

!!! pte
    Plus précisément, si $f$ est une fonction dérivable sur un intervalle $I=[a; b]$ :  

    * Si $f'> 0$[^1] sur l'intervalle ouvert $]a;b[$, alors $f$ est **strictement croissante** sur $[a;b]$  
    $^{[1]}$ (sauf éventuellement en un nombre fini de points où $f'$ peut s'annuler)  
    [^1]: (sauf éventuellement en un nombre fini de points où $f'$ peut s'annuler)  
    * Si $f'<0$[^1] sur l'intervalle ouvert $]a;b[$, alors $f$ est **strictement décroissante** sur $[a;b]$  
    $^{[1]}$  (sauf éventuellement en un nombre fini de points où $f'$ peut s'annuler)
    
!!! exp "Étude de la fonction $f(x)=x^3$"
    Soit $f(x) = x^3$, $f$ étant définie sur $\m R$, alors $f$ est dérivable sur $\m R$, et $f'(x) = 3x^2$, $f'(x) > 0$ sur tout $\m R$ (sauf en un seul point $x = 0$ pour lequel $f'(0)=0$) donc $f$ est strictement croissante sur $\m R$.  
    D'où le tableau de variations :  
    ![Tableau de Variation x3](./img/tv_x3.png){.center}

!!! exp "Étude de la fonction $f(x) = \dfrac 13x^3-\dfrac 32x^2+x+2$"
    Soit <enc>$f(x) = \dfrac 13x^3-\dfrac 32x^2+x+2$</enc>, Alors $f$ est dérivable sur $\m R$, et :  
    On a <enc>$f'(x)=x^2– 3x +1$</enc>, et $\Delta= (-3)^2 – 4\times1\times1 = 9 – 4 = 5 > 0$  
    L'équation $f'(x) = 0$ admet donc deux racines :
    <center>$x_1 = \dfrac {-b-\sqrt \Delta}{2a}=\dfrac {3-\sqrt 5}{2}\approx 0,382$</center> et 
    <center>$\quad x_2 = \dfrac {-b+\sqrt \Delta}{2a}=\dfrac {3+\sqrt 5}{2}\approx 2,618$</center>
    De plus, on sait que le trinôme du second degré $f'(x)$ est :  
    
    * du signe de $-a=-1<0$, donc négatif, entre les racines $x_1$ et $x_2$, donc la fonction $f$ est décroissante sur $I = \left] \dfrac {3-\sqrt 5}{2}; \dfrac {3+\sqrt 5}{2} \right[$, et 
    * du signe de $a=1>0$, donc positif ailleurs, (càd) à l'extérieur des racines, donc la fonction $f$ est croissante sur $J = \left]-\infty ; \dfrac {3-\sqrt 5}{2}\right[ \cup \left]\dfrac {3+\sqrt 5}{2} ; +\infty\right[$  
    D'où le tableau de variations suivant :  
    ![Tableau de Variation x3](./img/tv_exemple2_cours.png){.center}

    <iframe id="cubique" src="https://www.geogebra.org/classic/cwsdyrwx?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;margin-top:1em;" frameborder="0"></iframe>



!!! notation
    Dans toute la suite, on convient toujours que:

    * une flèche **montante** $\nearrow$ sur un intervalle $I$ signifie que la fonction est **strictement croissante** sur $I$
    * une flèche **descendante** $\searrow$ sur un intervalle $I$ signifie que la fonction est **strictement décroissante** sur $I$
    * <env>:warning: DE PLUS (SPOILER) :warning:</env> Dans un tableau de variation, on convient également qu'une flèche sur un intervalle $I$ représente une fonction $f$ qui est forcément **continue** sur $I$ (càd intuitivement, une fonction dont on peut tracer la courbe $\mc C$ sur l'intervalle $I$, **SANS LEVER LE STYLO**, notion qui sera étudiée en Terminale)

!!! ex
    Étudier les variations des fonctions suivantes et résumer dans un tableau de variation :
    
    1. $f(x)= \dfrac {x^2-3x+4}{5x-1}$
    1. $f(x)= \dfrac {1}{x^3+2x^2-3x+5}$


## Notion d'Extremum Local

Reprenons la fonction $f$ définie par : $f(x) = \dfrac 13x^3-\dfrac 32x^2+x+2$, et considérons sa courbe $\mc C$ (cf. le tableau de variations de l'exemple précédent).

###  Notion de Maximum local

Pour traduire le fait que le point $A$ est **<em>localement</em> le point le plus haut de la courbe $\mc C$** (localement signifie intuitivement '*autour de $x_1$*'), on dit en mathématiques que l'ordonnée $f(x_1)$ du point $A$ est un **maximum local de la courbe**. 

Nous allons tâcher de parvenir à une définition plus précise d'un maximum local d'une courbe. Pour cela, commençons par les définitions suivantes :

#### Majorant

!!! def "Majorant"
    Soit $D$ une partie de $\m R$.  
    Le nombre (réel) <red>$M$ est un majorant de $f$ sur $D$</red> $\quad \ssi$ $\forall x\in D, \quad f(x) \le M$  

!!! pte "Interprétation Graphique"  
    $M$ est <red>un majorant de $f$ sur $D$</red> $\ssi$ la courbe $\mc C$ est située **en dessous** de la droite horizontale d'équation : $y = M$, du moins dans la partie de la courbe correspondant à la partie $D$ de l'axe des $x$.

!!! exp "de Majorant, et Infinité de Majorants"
    Dans la figure ci-dessous, sur l'intervalle $D = \left[0; 2 \right]$, on peut dire que :

    * $-2$ est un majorant de $f$ sur $\left[0 ;2\right]$
    * $-1$, $1$ et $2$ sont trois autres majorants de $f$ sur $\left[0 ;2\right]$

    <env>**Extrapolation**</env> S'il existe un majorant $M$ de $f$ sur $D$ alors il en existe une infinité…  

<iframe id="majorant" src="https://www.geogebra.org/classic/twjhgduj?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

#### Majorant atteint en $x_0$

!!! def "Majorant atteint en $x_0$"
    On dit qu' <red>un majorant $M$ est atteint en $x_0$</red> lorsque :
    
    * $M$ est (bien) un majorant de $f$ sur $D$, et que 
    * ce majorant $M$ est **atteint** en $x_0$, càd que <enc>$f(x_0)=M$</enc>

!!! remarque
    Un majorant **PEUT** être atteint en un certain réel $x_0$, **OU PAS**: ce n'est pas une obligation.

!!! exp "Majorant atteint en $x_0$, ou pas"
    On dit que le majorant $-2$ est ***atteint*** car il existe un réel $x_0$ tel que : $f(x_0) = -2$  (ici : $f(1) = -2$)  
    Par contre, aucun des trois autres majorants envisagés (ni $-1$, ni $1$, ni $2$) ne sont atteints :
    En effet (par ex.) l'équation $f(x) = -1$ n'admet pas de solutions (ni $f(x) = 1$, ni $f(x) = 2$ ).  

#### Maximum, Maximum local

!!! def "Maximum, Maximum local"
    Soit $D$ une partie de $\m R$.  

    1. On dit que $M$ est <red>un maximum de $f$ sur $D$, atteint en $x_0$</red> si seulement si :

        * $M$ est un majorant de $f$ sur $D$, càd $\forall x\in \m R, \quad f(x) \le M$, et
        * Ce majorant $M $est atteint en un réel $x_0$ de $D$, càd : $\exists x_0\in D$ tel que $f(x_0) = M$  

    1. On dit que $M$ est <red>un maximum local de $f$</red> $\ssi$ il existe un intervalle ouvert $I$ tel que $M$ soit un maximum de $f$ sur $I$

!!! remarque
    On parle également de maximum <red>de la courbe $\mc C$</red> (qu'il soit local, ou pas)

!!! exp
    Par exemple, $-2$ est un maximum de $f$ sur $\left[0 ;2\right]$ et il est atteint en $x_0 = 1$.  
    Par contre, ni $-1$, ni $1$, ni $2$, ne sont des maxima de $f$ sur $\left[0 ;2\right]$ car aucun d'entre eux n'est atteint.

!!! remarque
    Maximum est un mot latin, on dit: un **maximum**, deux **maxima**

!!! remarque
    Contrairement à un majorant, un maximum (qu'il soit local, ou pas) est **forcément atteint**.

### Notion de Minimum local

Reconsidérons un instant la fonction $f$ définie par : $f(x) = \dfrac 13x^3-\dfrac 32x^2+x+2$   (cf. la courbe ci-dessous)  
Pour traduire le fait que le point $B$ est **localement le point le plus bas de la courbe** (localement signifie intuitivement '*autour de $x_2$*'), on dit que l'ordonnée $f(x_2)$ du point $B$ est un **minimum local de la courbe**.  

<iframe id="minimum" src="https://www.geogebra.org/classic/tbtmgajz?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>


Nous allons tâcher de parvenir à une définition plus précise de minimum local. Pour cela, commençons par les définitions suivantes :

#### Minorant

!!! def
    Soit $D$ une partie de $\m R$.  
    Le nombre (réel) $m$ est red>un minorant de $f$ sur $D$</red> $\ssi$ $\forall x\in D, \quad f(x)\ge m$

!!! pte "Interprétation Graphique"
    $m$ est un minorant de $f$ sur $D$ $\ssi$ la courbe $\mc C$ est située **au dessus** de la droite horizontale d'équation : $y = m$, du moins dans la partie de la courbe correspondant à la partie $D$ de l'axe des $x$.

!!! exp
    Dans la figure ci-dessous, sur l'intervalle $D =\left[1 ;3\right]$, on peut dire que :

    * $4$ est un minorant de $f$ sur $\left[1 ;3\right]$
    * $3$, $2$ et $-1$ sont trois autres minorants de $f$ sur $\left [1 ;3\right]$
    
    <env>Extrapolation</env> S'il existe un minorant $m$ de $f$ sur $D$, alors il en existe une infinité…

<iframe id="minorant" src="https://www.geogebra.org/classic/fufmbvds?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>


#### Minorant atteint en $x_0$

!!! def "Minorant atteint en $x_0$"
    On dit qu' <red>un minorant $M$ est atteint en $x_0$</red> lorsque :
    
    * $M$ est (bien) un minorant de $f$ sur $D$, et que 
    * ce minorant $M$ est **atteint** en $x_0$, càd que <enc>$f(x_0)=M$</enc>

!!! remarque
    Un minorant **PEUT** être atteint en un certain réel $x_0$, **OU PAS**: ce n'est pas une obligation.

!!! exp
    Le minorant $4$ est **atteint**, c'est-à-dire qu'il existe un réel $x_0$ tel que : $f(x_0) = 4$  (ici : $f(2) = 4$)  
    Aucun des trois autres minorants envisagés (ni $3$, ni $2$, ni $-1$) ne sont atteints :
    Cela veut dire que (par ex.) que l'équation $f(x) = 3$ n'admet pas de solutions (ni $f(x) = 2$, ni $f(x) = -1$ )

#### Minimum, Minimum Local

!!! def "Minimum, Minimum Local"
    Soit $D$ une partie de $\m R$.  

    1. On dit que $m$ est <red>un minimum de $f$ sur $D$, atteint en $x_0$</red> si et seulement si :  

        * $m$ est un minorant de $f$ sur $D$, càd $\forall x\in D, \quad f(x) \ge m$
        * Ce minorant $m$ est atteint en un réel $x_0$ de $D$, càd $\exists x_0\in D$ tel que $f(x_0) = m$

    1. On dit que $m$ est <red>un minimum local de $f$</red> $\ssi$ il existe un intervalle ouvert $I$  tel que $m$ soit un minimum de $f$ sur $I$

!!! remarque
    On parle également de minimum <red>de la courbe $\mc C$</red> (qu'il soit local, ou pas)

!!! exp
    Par exemple, $4$ est un minimum de $f$ sur $\left[1 ;3\right]$ et il est atteint en $x_0 = 2$.  
    Par contre, ni $3$, ni $2$, ni $-1$ ne sont des minima de $f$ sur $\left[1;3\right]$ car aucun d'entre eux n'est atteint.

!!! remarque
    Contrairement à un minorant, un minimum (qu'il soit local, ou pas) est **forcément atteint**.

!!! remarque
    Minimum est un mot latin, on dit: un **minimum**, deux **minima**

### Notion d' Extremum Local

!!! def "Extremum Local"
    * On dit que que $f(x_0)$ est <red>un extremum de $f$ sur $D$</red> lorsque :
        * ou bien $f(x_0)$ est un minimum de $f$ sur $D$ 
        * ou bien $f(x_0)$ est un maximum de $f$ sur $D$ 
    * On dit que $f(x_0)$ est <red>un extremum local de $f$</red> (donc atteint en $x_0$) lorsque :
        * ou bien $f(x_0)$ est un minimum local de $f$ (donc atteint en $x_0$)
        * ou bien $f(x_0)$ est un maximum local de $f$ (donc atteint en $x_0$) 

!!! remarque
    On parle également d'extremum <red>de la courbe $\mc C$</red> (qu'il soit local, ou pas)

!!! remarque
    Extremum est un mot latin, on dit: un **extremum**, deux **extrema**


## Extrema Locaux et Racines de l'équation $f'(x) = 0$

D'après le graphique de la courbe de la fonction $f(x)=\dfrac 13x^3-\dfrac 32x^2+x+2$ plus haut sur cette page web, on peut concevoir que :

!!! pte
    Soit $f$ une fonction dérivable sur un intervalle $I$. On note $\mc C$ sa courbe.  

    * Si $\begin{cases}{}
    f(x) \text{ est un extremum local à la courbe
     } \mc C \\
    \text{\redbox{ET}} x \text{ n'est pas une extrémité de } I
    \end{cases}
    $ $\quad$ alors <enc>$f'(x) = 0$</enc>

    * Si $f(x)$ est un extremum local à la courbe $\mc C$, alors $\mc C$ admet une tangente horizontale en $M\coord x{f(x)}$

On aimerait bien que cette condition nécessaire $f'(x) = 0$ soit aussi suffisante, c'est-à-dire on aimerait bien que l'égalité $f'(x)=0$ implique que $f(x)$ soit un extremum local, **malheureusement c'est faux** : il est (malheureusement) possible d'avoir $f'(x) = 0$ mais que $f(x)$ ne soit pas un extremum local de $\mc C$. En-voici un contre-exemple :

!!! exp "Un Contre- exemple"
    Considérons de nouveau $f(x) = x^3$, qui est dérivable sur $\m R$, et on a $f'(x) = 3x^2$.  
    On peut dire que :

    1. D'une part, **La dérivée $f'$ s'annule en $x=0$** (càd $f'(0) = 0$) :  
    En effet, Résolvons l'équation : $f'(x) = 0 \ssi x^2 = 0 \ssi x = 0$  
    En particulier, la tangente à $\mc C$ au point $A(0 ; 0)$ est bien horizontale. 
    1. Malheureusement, ce n'est pas suffisant pour que $f(0)=0$ soit un extremum local de $\mc C$ en $x=0$. Le problème vient du fait que $\forall x\in \m R, f'(x) = 3x^2\ge 0$ donc la dérivée $f'$ est toujours positive, en particulier **la dérivée $f'$ ne change pas de signe en $x = 0$**.  
    On peut aussi bien le voir avec le tableau de variations de f :
    ![Tableau de Variation de x^3](./img/tv_x3_n2.png){.center}

    <iframe id="courbex3" src="https://www.geogebra.org/classic/ppg7cmzr?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

!!! thm "Racines de $f'(x) = 0$ et Extrema locaux"
    Soit $f$ une fonction dérivable sur un intervalle $I$ ouvert et contenant un réel x de $I$.
    $
    \begin{cases} {}
    f'(x)=0 \quad\quad\quad \\
    \text{\redbox{ET} la dérivée } f' \text {change de signe en } x
    \end{cases}
    $ $\ssi f(x)$ est un extremum local (atteint en $x$)  
    Dans ce cas, la tangente à la courbe $\mc C$ au point $A \coord {x}{f(x)}$ est horizontale.  

!!! morale
    Attention bien à vérifier que la dérivée $f'$ **change bien de signe** en une racine de l'équation $f'(x) = 0$, sinon cette racine ne sera pas un extremum local.

!!! ex
    Pour chacune des fonctions $f$ suivantes, étudier les variations et déterminer les extrema locaux (lorsqu'il y en a).

    1. $f(x) = –x^2 + 3x – 5$	
    2. $f(x) = 3x^3 –2x^2 –x + 1$  
    3. $f(x) = \dfrac {1}{x^3+2x^2-2x+1}$  
    4. $f(x) = \dfrac {x+1}{x^2+3x+4}$  
    5. $f(x) = \dfrac {x^2+3x+2}{x^2+x-12}$  

