# 1MATHS : Introduction aux Suites

## Un Exemple et Vocabulaire

!!! def
    Une <red>suite</red> de nombres, notée <enc>$u$</enc> ou <enc>$(u_n)$</enc> , est une succession de plusieurs nombres placés dans un certain ordre

!!! exp "(de suite)"
    $3$ ; $14$ ; $5$ ; $8$ ; $7$ ; etc...

!!! def "terme"
    Chacun des nombres $u_n$ de cette suite est appelé <red>un terme</red> de la suite
    
!!! exp "(de termes)"
    * $3$ est un **terme** de la suite précédente.  
    * $14$ est un (autre) **terme** de la suite précédente, situé près le précédent  
    * $5$ est un (autre) **terme** de la suite précédente, situé près le précédent  

Cette **suite** de nombres/de termes doit être considérée comme **ordonnée**, ce qui revient à numéroter la position/la place/<red>le rang</red> de chacun des nombres/termes : il existe un *premier nombre*, puis un *deuxième nombre*, etc..

!!! def "rang d'une terme"
    Le <red>rang</red> d'un terme de la suite est sa position/place dans la suite

Dans notre exemple ci-dessus, on dit que :

!!! exp "$u_n$ est le terme de rang $n$"
    * *le premier de ces nombres* est $3$ : il est appelé <red>le premier terme</red> de la suite, ou <red>le terme de rang $1$</red>, ou <red>le terme initial</red>, il est *souvent* noté : 
        * $u_1 \quad$ (notation des suites) ou 
        * $u(1) \quad$ (notation fonctionnelle)
    * *le deuxième de ces nombres* est $14$ : il est appelé <red>le deuxième terme</red> de la suite, ou <red>le terme de rang $2$</red>. il est *souvent* noté :
        * $u_2 \quad$ (notation des suites) ou
        * $u(2) \quad$ (notation fonctionnelle)
    * *le troisième terme*, ou <red>le terme de rang $3$</red>, est $5$ : il est *souvent* noté :
        * $u_3 \quad$ (notation des suites) ou
        * $u(3) \quad$ (notation fonctionnelle)
    * etc...
    * Le <red>$n$-ième terme</red>, ou <red>le terme de rang $n$</red>, ou <red>le terme général</red>, que l’on trouve dans cette *suite* de nombres est noté :
        * $u_n \quad$ (notation des suites) ou 
        * $u(n) \quad$ (notation fonctionnelle)
    * etc.. on imagine qu'il peut y avoir une quantité **finie** ou **infinie** de nombres dans une *suite* (de nombres)

!!! pté
    Autrement dit, mathématiquement, on peut voir/modéliser une suite de nombres comme une fonction de l'ensemble $\m N$ dans l'ensemble $\m R$, qui à chaque position/rang $n$ associe le terme $u_n$ situé à cette position/rang :

    <center>$
    \begin{align*}
    u: \quad & \m N \rightarrow \m R \\
            & n \mapsto u(n)=u_n
    \end{align*}
    $</center>

    * Le nombre entier $n$ est la position/place/*rang* du nombre dans la suite, tandis que
    * $u_n$ est le nombre/terme qui se trouve à la position/place/rang $n$

!!! remarques
    1. On veillera à bien distinguer les deux notations suivantes :  

        * $u_n$ désigne uniquement UN SEUL nombre : celui situé au $n$-ième rang dans la suite, tandis que
        * $(u_n)$ désigne l'ensemble de TOUS les nombres de la suite, ce que l'on appelle *la suite*

    1. Terme initial <enc>$u_0$ vs $u_1$ vs $u_2$ vs ..</enc> : 
    
        * :warning: Le terme initial n'est pas obligatoirement noté $u_1$ :warning:
        * Certes, il arrive assez régulièrement que le terme initial soit noté $u_1$ ,
        * mais, il arrive également assez régulièrement que, selon les contextes, il soit noté $u_0$, voire $u_2$, etc..  
        Si l'on estime que cette notation coïncide mieux à la situation réelle que ce terme initial est censé représenter. Par exemple, on peut choisir de noter $u_0$ un certain montant placé sur un compte bancaire durant l'année $0$, càd avant la fin de la *première année*, $u_1$ désignerait alors le montant durant l'année $1$ (entre 12 mois et 24 mois), $u_2$ le montant durant l'année $2$ (entre 24 mois et 36 mois), etc..).  
        Dans le cas où le terme initial soit noté $u_0$ (et pas $u_1$), alors on dira que:

            * $u_0$ est le terme initial, $u_0$ est le terme de rang $0$, on encore (confusément) le **premier terme**
            * ($u_1$ n'est plus le terme initial..), $u_1$ est le terme de rang $1$, on encore (confusément) le **deuxième terme**.
            * etc..

