# Algorithmes et Exponentielle

|Algorithmes Dérivées|
|:-:|
|Construction de l’exponentielle par la méthode d’Euler.<br/>Détermination d’une valeur approchée de $e$ à l’aide de la suite $((1+\dfrac 1n)^n)$ |

