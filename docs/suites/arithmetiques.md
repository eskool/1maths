# 1MATHS : Suites Arithmétiques

## Définition

!!! def
    Une suite $(u_n)$ est dite <red>arithmétique de raison $r$</red>, avec $r \in \mathbb R$, lorsque:  
    <center>Pour tout entier $n$, $\quad u_{n+1}=u_n+r$</center>

!!! exp
    Pour tout entier $n$, $\quad u_{n+1}=u_n+4$ avec $u_0=1$

    * La suite $(u_n)$ est une suite arithmétique de raison $r=4$, et de terme initial $u_0=1$
    * Calculer les premiers termes: $u_1$, $u_2$, $u_3$, $u_4$  
    On a : $u_1 = u_0 + r = 1 + 4= 5$  
    On a : $u_2 = u_1 + r = 5 + 4 = 9$  
    On a : $u_3 = u_2 + r = 9 + 4 = 13$  
    On a : $u_4 = u_3 + r = 13 + 4 = 17$  

## Comment montrer qu'une suite est arithmétique, ou pas

!!! pte
    Une suite $(u_n)$ est arithmétique, de raison $r$
    $\Leftrightarrow$ la suite de toutes les différences  $u_{n+1}-{u_n}$ est constante, et vaut (toujours) $r$

!!! ex "Montrer qu'une suite n'est PAS arithmétique"
    Soit $(u_n)$ la suite définie par $u_n=n^2$  
    Montrer que la suite $(u_n)$ n'est pas arithmétique

    ??? corr
        Calculons:

        * $u_0=0^2=0$
        * $u_1=1^2=1$
        * $u_2=2^2=4$
        * $u_3=3^2=9$

        Calculons les différences suivantes:

        * $u_1-u_0 = 1-0 = 1$
        * $u_2-u_1 = 4-1 = 3$

        Conclusion : $u_2-u_1 \ne u_1-u_0$  
        Ceci prouve que PAS toutes les différences $u_{n+1}-u_n$ ne sont égales entre elles.  
        donc que la suite $(u_n)$ n'est PAS arithmétique

!!! ex "Montrer qu'une suite EST arithmétique"
    Soit $(u_n)$ la suite définie par $u_n=5n+3$ pour tout entier $n$  
    Montrer que la suite $(u_n)$ est arithmétique.  
    On déterminera la raison $r$, et le terme initial $u_0$

    ??? corr
        Commençons par exprimer $u_{n+1}$ en fonction de $n$ (nous en aurons besoin):  
        Pour tout entier $n$, $u_{n+1}=5\times (n+1)+3=5n+5+3=5n+8$  
        Calculons les différences suivantes en fonction de $n$:  
        $u_{n+1}-u_n = (5n+8)-(5n+3) = 5n+8-5n-3 = 8-3 = 5$  
        Ceci prouve que:

        * la suite $(u_n)$ est arithmétique
        * sa raison vaut $r=5$
        * le terme initial vaut $u_0=5\times 0 + 3= 3$

## Exprimer $u_n$ en fonction de $n$

!!! pte
    Soit $(u_n)$ une suite arithmétique, de raison $r$.  
    Pour exprimer $u_n$ en fonction de $n$, on dispose des formules suivantes:

    * $u_n=u_0 + n\times r$
    * $u_n=u_1 + (n-1)\times r$
    * $u_n=u_2 + (n-2)\times r$
    * etc...
    * $u_n=u_p + (n-p)\times r$ où $p$ est un entier positif.

!!! ex
    1. Soit $(u_n)$ une suite arithmétique de raison $r=2$, et de terme initial $u_0=3$.  
    Exprimer $u_n$ en fonction de $n$  

        ??? corr
            On sait que $u_n = u_0 +n\times r = 3 + n\times 2 = 2n+3$

    2. Soit $(u_n)$ une suite arithmétique de raison $r=4$, et de terme initial $u_1=5$.  
    Exprimer $u_n$ en fonction de $n$

        ??? corr
            On sait que $u_n = u_1 +(n-1)\times r = 5 + (n-1)\times 4 = 4n-4+5 = 4n+1$

## Somme des termes consécutifs d'une suite arithmétique

!!! pte
    Pour tout entier $n\ge 1$,
    <center>$1+2+3+...+n=\dfrac {n(n+1)}{2}$</center>

??? preuve
    $\begin{align*}
    S &= &1 &+ &2 &+ &3 &+ ... &+ &n \\
    +S &= &n &+ &(n-1) &+ &(n-2) &+ ... &+ &1 \\
    \rule{2cm}{0.4pt} \\
    =2S &= &(n+1) &+ &(n+1) &+ &(n+1) &+ ... &+ &(n+1) \\
    =2S &= n\times (n+1)
    \end{align*}
    $  

    donc

    $S=\dfrac {n(n+1)}{2}$


!!! ex
    Calculer les sommes suivantes:

    1. $S=1+2+3+..+2^{10}$

        ??? corr
            $\begin{align*}
            S &= 1+2+4+..+2^{10}=1+2+2^2+..+2^{10} \\
                &= \dfrac {2^{10+1}-1}{2-1} \\
                &= \dfrac {2^{10+1}-1}{1}=2^{11}-1 \\
                &= 2047
            \end{align*}
            $

    2. $S=1+3+9+..+3^{24}$

        ??? corr
            $S=1+3+3^2+..+3^{24}=\dfrac {3^{24+1}-1}{3-1}=\dfrac {3^{25}-1}{2}$

    3. $S=1+\dfrac {1}{2}+\dfrac {1}{4}+..+\dfrac {1}{2^{30}}$

        ??? corr
            $S= 1+\dfrac {1}{2}+\dfrac {1}{2^2}+..+\dfrac {1}{2^{30}}$  
            $S= 1+\dfrac {1}{2}+\left( \dfrac {1}{2} \right)^2+..+\left( \dfrac {1}{2} \right)^{30}$ donc $q=\dfrac 1 2$ et $n=30$  
            $S= \dfrac {1 - \left( \dfrac 1 2 \right) ^{30+1}}{1 - \dfrac 1 2}= \dfrac {1- \left( \dfrac 1 2 \right) ^{31}}{\dfrac 1 2}$  
            $S= 2\times \left( 1- \left( \dfrac 1 2 \right) ^{31} \right)$  
            $S= 2\times \left( 1- \dfrac 1 {2^{31}} \right)$

    4. Exprimer $S$ en fonction de $n$ avec $S=1+2+4+...+2^{n}$
    
        ??? corr
            $\begin{align*}
            S &= 1+2+4+..+2^{n} \\
                &= 1+2+2^2+..+2^{n} \\
                &= \dfrac {2^{n+1}-1}{2-1} \\
                &= \dfrac {2^{n+1}-1}{1} \\
                &= 2^{n+1}-1
            \end{align*}$

    5. Exprimer $S$ en fonction de $n$ avec $S=1+3+9+..+3^{n}$

    6. Exprimer $S$ en fonction de $n$:
    $S=1+\dfrac {1}{2}+\dfrac {1}{4}+..+\dfrac {1}{2^{n}}$

    7. Exprimer $S$ en fonction de $n$:
    $S=1+\dfrac {2}{3}+\dfrac {4}{9}+..+\dfrac {2^n}{3^{n}}$

    8. Exprimer $S$ en fonction de $n$:
    $S=2^{10}+2^{11}+..+2^n$

    9. Exprimer $S$ en fonction de $n$:
    $S=\dfrac {2^4}{3^4}+\dfrac {2^5}{3^5}+..+\dfrac {2^n}{3^{n}}$


!!! pte "Somme commençant par $u_0$"
    Soit $(u_n)$ une suite arithmétique de raison $r$ et de terme initial $u_0$:  
    <center><enc>$\begin{align*}S = u_0+u_1+u_2+...+u_n &= (n+1)\times \dfrac {u_0+u_n}{2} \\
        &=\text{(NB de Termes)} \times \dfrac {u_0+u_n}{2} \\  
        &=\text{(NB de Termes)} \times \dfrac {\text{Somme des Deux extrémités}}{2} \\
    \end{align*}
    $</enc></center>

!!! pte "Somme commençant par $u_a$"
    Soit $(u_n)$ une suite arithmétique de raison $r$ (et de terme initial $u_0$ (ou bien, au pire, égal à $u_a$):
    <center><enc>$
    \begin{align*}
    S = u_a+u_{a+1}+...+u_b &= (b-a+1)\times \dfrac {u_a+u_b}{2} \\
        &= \text{(NB de Termes)} \times \dfrac {u_a+u_b}{2} \\
        &= \text{(NB de Termes)} \times \dfrac {\text{Somme des Deux extrémités}}{2}
    \end{align*}
    $</enc></center>

## Limite des suites $(r\times n)$

!!! pte
    Soit $r$ un réel.

    :one: Si $r<0$, Alors $\displaystyle \lim_{n \to +\infty} rn=-\infty$  
    :two: Si $r>0$, Alors $\displaystyle \lim_{n \to +\infty} rn=+\infty$  
    :three: Si $r=0$, Alors $\displaystyle \lim_{n \to +\infty} 0\times n=0$

!!! ex
    Déterminer la limite des suites $(u_n)$ suivantes

    1. $u_n=0,3\times n$
    
        ??? corr
            $\displaystyle \lim_{n \to +\infty} 0,3\times n=+\infty$ car $r=0,3>0$

    2. $u_n=5,6\times n$

        ??? corr
            $\displaystyle \lim_{n \to +\infty} 5,6\times n=0$ car $r=5,6>0$

    3. $u_n=-0,07\times n$

        ??? corr
            $\displaystyle \lim_{n \to +\infty} -0,07\times n=-\infty$ car $r=-0,07<0$

    4. $u_n=14,5\times n$

        ??? corr
            $\displaystyle \lim_{n \to +\infty} 14,5\times n=+\infty$ car $r=14,5>0$

    5. $u_n=-2n$
    
        ??? corr
            $\displaystyle \lim_{n \to +\infty} -2\times n=-\infty$ car $r=-2>0$

## Limite de la Somme des termes consécutifs d'une suite Géométrique

!!! pte
    Soit $(u_n)$ une suite arithmétique de raison $r$  
    Soit $S_n=u_0+u_1+...+u_n$
    Alors $\displaystyle \lim_{n \to +\infty} S_n=\dfrac {u_0} {1-q}=u_0\times \dfrac {1}{1-q}$

<demonstration bo>

Soit $S_n=u_0+u_1+u_2+...+u_n=u_0+u_0\times q^1+u_0\times q^2+...+u_0\times q^n$
donc $S_n=u_0\times \left( 1+q^1+q^2+...+ q^n\right)= u_0\times \dfrac{1-q^{n+1}}{1-q}$
$S_n= \dfrac{u_0}{1-q}\times (1-q^{n+1})$
Or $0<q<1$ donc $\displaystyle \lim_{n \to +\infty} q^{n+1}=0$ donc $\displaystyle \lim_{n \to +\infty} (1-q^{n+1})=1-0=1$ d'après le théorème sur la soustraction de limites ("TSL")
Donc $\displaystyle \lim_{n \to +\infty} \dfrac{u_0}{1-q}\times (1-q^{n+1})=\dfrac{u_0}{1-q}\times 1=\dfrac{u_0}{1-q}$
d'après le théorème sur le produit de limites ("TPL")
donc $\displaystyle \lim_{n \to +\infty} u_n=\frac{u_0}{1-q}$

</demonstration>

<exo>

Pour chacune des suites **géométriques** suivantes, calculer la limites de la somme $S_n$ indiquée:

1. $(u_n)$ géom, de raison $q=\dfrac 12 $, $u_0=3$, $S_n=u_0+u_1+...+u_n$
<br/>
2. $(u_n)$ géom, de raison $q=\dfrac 23 $, $u_0=4$, $S_n=u_0+u_1+...+u_n$
<br/>
3. $(u_n)$ géom, de raison $q=0,48 $, $u_0=6$, $S_n=u_0+u_1+...+u_n$
<br/>

</exo>