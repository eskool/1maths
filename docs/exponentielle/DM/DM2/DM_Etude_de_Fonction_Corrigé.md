<titre><red>Corrigé</red> DM Étude de fonction avec une Exponentielle</titre>

<ex>

Soit $f$ la fonction définie sur $I=\mathbb R$, par $f(x)= (3x+2) \times e^{0.6x}$

1. Calculer la fonction dérivée $f'(x)$. Puis, Factoriser $f'(x)$ du mieux que vous pouvez

    $f'(x)=3e^{0,6x}+(3x+2)\times0,6 e^{0,6x}$
    $f'(x)=e^{0,6x}(3+0,6(3x+2))$
    <enc>$f'(x)=e^{0,6x}(1,8x+4,2)$</enc>

2. Étudier le signe de $f'(x)$, autrement dit : Résoudre l'inéquation $f'(x)>0$

    $f'(x)\ge 0$
    $\Leftrightarrow 1,8x+4,2 \ge 0$ car l'exponentielle est strictement positive sur $\mathbb R$
    $\Leftrightarrow 1,8x \ge -4,2$
    $\displaystyle \Leftrightarrow x \ge \frac {-4,2}{1,8}$
    $\displaystyle \Leftrightarrow x \ge -\frac 73 \approx -2,33$

3. En déduire le Tableau de Variation de $f$ sur $\mathbb R$

    | $x$ | $-\infty$| $-\frac 73$ | $+\infty$ |
    |:-:|:-:|:-:|:-:|
    | $f'(x)$ | $-$ | $+$ |
    | $f$ | $\searrow$ | $\nearrow$ |

4. Étudier les extrema locaux $m$ sur $\mathbb R$:

    $m=f(x)$ est un extremum local en $x$
    $\Leftrightarrow$ la dérivée $f'$ s'annule et change de signe en $x$
    $\Leftrightarrow x= -\frac 73$ d'après le tableau de variations
    $m=f(-\frac 73)$ est donc l'**unique extremum** de $f$.
    De plus, cet extremum est un minimum, car :
    * $f'(x)<0$ pour $x < -\frac 73$
    * $f'(x)>0$ pour $x > -\frac 73$
    
    L'unique minimum de $f$ vaut donc $m=f(-\frac 73)=(3\times -\frac 73 + 2)\times e^{0,6\times -\frac 73} = -5\times e^{-1,4} \approx -1,23$
    Il est atteint en $x=-\frac 73$

5. Faites un graphique (approximatif) de la courbe de $f$ dans un repère. Unités: 

    * Une unité sur l'axe des $x$ correspond à $1$ cm.
    * Une unité sur l'axe des $y$ correspond à $1$ cm.

</ex>