# 1MATHS : Algorithmes et Mathématiques

Les algorithmes peuvent être écrits :

* en langage naturel ou **pseudocode**
* le langage **Python**

On utilise le symbole $\,$ <enc>$\leftarrow$</enc> $\,$ pour désigner l’affection dans un algorithme écrit en langage naturel.  
On consolide les notions de :

* variable, 
* d’instruction conditionnelle et 
* de boucle `for` et `while`
* l’utilisation des fonctions

|Algorithmique|
|:-:|
|Générer une liste (en extension, par ajouts successifs ou en compréhension).|
|Manipuler des éléments d’une liste (ajouter, supprimer...) et leurs indices.|
|Parcourir une liste.|
|Itérer sur les éléments d’une liste.|
