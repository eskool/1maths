# 1MATHS : Cours Polynômes/Trinômes du Second Degré

## Polynôme/Trinôme du Second Degré

### Définition

!!! def "Fonction Polynôme/Trinôme du Second Degré"
    Soit $a$, $b$ et $c$ trois nombres réels quelconques, avec $a\neq0$.  
    1. On appelle <red>(fonction) polynôme/trinôme</red> du second degré, définie sur $\mathbb R$, une fonction $f$ pouvant être écrite sous la forme $f(x) = ax^2+bx+c$ pour tout réel $x$, où $a\neq0$, $b$ et $c$ sont des nombres réels **constants**.  
    2. Les constantes <enc>$a\neq0$</enc>, <enc>$b$</enc> et <enc>$c$</enc> sont appelées les <red>coefficients du trinôme</red> $ax^2+bx+c$.

### Différentes Écritures de Départ

On peut retrouver une écriture sous forme de fonction polynôme/trinôme du second degré, en partant initialement des écritures classiques suivantes. En effet, après modification de leur écriture, on peut toujours les ramener à un trinôme $ax^2+bx+c$ (avec $a\neq0$) :

1.  Formes Développées/Réduites $\quad$ <enc>$ax^2+bx+c$</enc> (avec $a\neq0$ )
    1. $f(x)=\redbox{2}x^2\bluebox{-7}x+\greenbox{4}$  
    Coefficients de la Forme Développée/Réduite : $\redbox{a=2}$, $\bluebox{b=-7}$, $\greenbox{c=4}$
    1. $f(x)=\redbox{-}x^2+\bluebox{5}x\greenbox{-1}$  
    Coefficients de la Forme Développée/Réduite : $\redbox{a=-1}$, $\bluebox{b=5}$, $\greenbox{c=-1}$
    1. $f(x)=\redbox{-3}x^2$  
    Coefficients de la Forme Développée/Réduite : $\redbox{a=-3}$, $\bluebox{b=0}$, $\greenbox{c=0}$
    1. $f(x)=4-x^2$ car $f$ peut être réordonnée en $f(x)=\redbox{-}x^2+\greenbox{4}$, donc  
    Coefficients de la Forme Développée/Réduite : $\redbox{a=-1}$, $\bluebox{b=0}$, $\greenbox{c=4}$
1. Formes Canoniques $\quad$ <enc>$a\left( x-\alpha\right)^2+\beta$</enc> (avec $a\neq0$ )
    1. $f(x)=(x-2)^2+3=\redbox{1}(x-\bluebox{2})^2+\greenbox{3}=(x^2-4x+4)+3=x^2-4x+7$  
    Coefficients de la Forme Développée/Réduite : $a=1$, $b=-4$, $c=7$  
    Coefficients de la Forme Canonique : $\redbox{a=1}$, $\bluebox{\alpha=2}$, $\greenbox{\beta=3}$  
    1. $f(x)=3(x+2)^2+4=\redbox{3}(x-\bluebox{-2})^2+\greenbox{4}=3(x^2+4x+4)+4=3x^2+12x+16$  
    Coefficients de la Forme Développée/Réduite : $a=3$, $b=12$, $c=16$  
    Coefficients de la Forme Canonique : $\redbox{a=3}, \bluebox{\alpha=-2}, \greenbox{\beta=4}$  
    1. $f(x)=\redbox{2}(x-\bluebox{1})^2\greenbox{-3}=2(x^2-2x+1)-3=2x^2-4x-1$  
    Coefficients de la Forme Développée/Réduite : $a=2$, $b=-4$, $c=-1$  
    Coefficients de la Forme Canonique : $\redbox{a=2}, \bluebox{\alpha=1}, \greenbox{\beta=-3}$  
    1. $f(x)=\redbox{-4}(x-\bluebox{2})^2+\greenbox{1}=-4(x^2-2x+4)+1=-4x^2+8x-15$  
    Coefficients de la Forme Développée/Réduite : $a=-4$, $b=8$, $c=-15$  
    Coefficients de la Forme Canonique : $\redbox{a=-4}, \bluebox{\alpha=2}, \greenbox{\beta=1}$  
1. Formes Factorisées $\quad$ <enc>$a(x-x_1)(x-x_2)$</enc> (avec $a\neq0$)
    1. $f(x)=(x+2)(x-5)= \redbox{1}(x-\bluebox{-2})(x-\greenbox{5}) = x^2+2x-5x-10=x^2-3x-10$  
    Coefficients de la Forme Développée/Réduite : $a=1$, $b=-3$, $c=-10$    
    Coefficients de la Forme Factorisée : $\redbox{a=1}$, $\bluebox{x_1=-2}$, $\greenbox{x_2=5}$    
    1. $f(x)=(4x-1)(3-2x)=12x-8x^2-3+2x=-8x^2+14x-3$  
    Coefficients de la Forme Développée/Réduite : $a=1$, $b=-3$, $c=-10$  
    Coefficients de la Forme Factorisée : Il faut un peu travailler ici...  
    $\begin{align*}
    f(x) &= (4x-1)(3-2x) \\
        &= 4\left(x-\dfrac 14\right)\times (-2)\left(\dfrac{3}{-2}+x\right) \\
        &= \redbox{-8}\left(x-\bluebox{\dfrac 14}\right)\left(x-\greenbox{\dfrac 32}\right)
    \end{align*}$  
    donc Coefficients de la Forme Factorisée : $\redbox{a=-8}$, $\bluebox{x_1=\dfrac 14}$, $\greenbox{x_2=\dfrac 32}$ 
1. Autres Formes :
    1. $f(x)=(2x+1)^2-3x^2$, car $f(x)=4x^2+4x+1-3x^2=x^2+4x+1=\redbox{1}x^2+\bluebox{4}x+\greenbox{1}$
    Coefficients de la Forme Développée/Réduite : $\redbox{a=1}$, $\bluebox{b=4}$, $\greenbox{c=1}$  

<env>:warning: Par contre :warning:</env>, les fonctions suivantes ne sont PAS des trinômes du second degré :  

1. $f(x)=(x+3)^2-x^2$, car en développant $f(x)=(\cancel{x^2}+6x+9)-(\cancel{x^2})=6x+9$  
Autrement dit, dans ce cas : $a=0$
1. $f(x)=(2x-1)(x+5)-(2x^2-4)$, car en développant $f(x)=(\cancel{2x^2}+10x-x-5)-(\cancel{2x^2}-4)=9x-1$  
Autrement dit, dans ce cas : $a=0$

## Forme Développée/Réduite d'un Trinôme du Second degré

!!! def "Forme Développée/Réduite d'un Polynôme du Second Degré"
    L'écriture sous la forme <enc>$ax^2+bx+c$</enc>, càd développée, réduite et ordonnée, est appelée la <red>Forme Développée</red>, ou quelquefois la <red>Forme Réduite</red> du trinôme.  

| Exemple | Exercice |
|:-:|:-:|
| Soit $f(x)= \redbox{2}x^2\bluebox{-5}x+\greenbox{3}$ |Soit $f(x)=–3x^2+2x–5$|
| > | Cette fonction est une fonction trinôme<br/> dont les coefficients sont : |
| $\redbox{a=2}$, $\bluebox{b=-5}$, et $\greenbox{c=3}$| $a=$_______; $b=$_______; $c=$________ |

## Discriminant $\Delta$ d'un Trinôme

!!! def
    On se donne un trinôme $\redcolor{a}x^2 + \bluecolor{b}x + \greencolor{c}$  
    Le nombre réel <enc>$\Delta=\bluecolor{b}^2-4\redcolor{a}\greencolor{c}$</enc> $\,\,$ est appelé le <red>discriminant</red> du trinôme $ax^2 + bx + c$.

| Exemple (Suite) | Exercice (Suite) |
|:-:|:-:|
| Soit $f(x) = \redcolor{2}x^2\bluebox{\bluecolor{-5}}x+\greencolor{3}$ | Soit $f(x) = –3x^2+2x–5$ |
| > | Alors le discriminant $\Delta$ de ce trinôme vaut : |
|$\Delta= \bluecolor{b}^2-4\redcolor{a}\greencolor{c}$ <br/>$=\bluecolor{(-5)}^2-4\times(\redcolor{2}) \times(\greencolor{3})$<br/>$=25-24$,<br/>$\Delta=1$ | $\Delta=b^2-4ac$<br/>$=$_______________________________<br/>$=$_______________________________<br/>$=$_______________________________ |

## Forme Canonique d'un Trinôme du Second Degré

!!! thm "Transformation de la forme réduite $ax^2 + bx + c$ en une 'Forme Canonique'"
    TOUT trinôme $f(x)=ax^2 + bx + c$ (avec $a\neq0$), peut s'écrire sous la <red>Forme Canonique</red> suivante :  

    \begin{align*}
    ax^2+bx+c &= a\left(x+\dfrac b{2a}\right)^2 - \dfrac {\Delta}{4a} \\
              &= a\left(x-\alpha\right)^2 + \beta \quad \text{avec} \quad \alpha=-\dfrac {b}{2a} \quad \text{et} \quad \beta=f(\alpha)=\dfrac {-\Delta}{4a}
    \end{align*}
 
??? démo
    Soit $\Delta=b^2-4ac$ , le discriminant du trinôme $f(x)=ax^2 + bx + c$, avec $a\neq 0$.  
    :one: Développons :

    \begin{align*}
    a \left( x + \dfrac {b}{2a} \right)^2 &= a \left( x^2 + 2x\times \dfrac {b}{2a} + \left( \dfrac {b}{2a}\right)^2 \right) \\
        &= ax^2 + \cancel{2}\cancel{a}x\times \dfrac {b}{\cancel{2}\cancel{a}} + \cancel{a}\times \dfrac {b^2}{4\cancel{a}\times a} \\
        &= ax^2 + bx + \dfrac {b^2}{4a}
    \end{align*}

    :two: Transformons cette écriture :

    \begin{align*}
    a \left( x + \dfrac {b}{2a} \right)^2 &= ax^2 + bx + \dfrac {b^2}{4a} \\
        &= ax^2 + bx + \cancel{c} -\cancel{c} + \dfrac {b^2}{4a} \\
        &= (ax^2 + bx + c) + \left(\dfrac {b^2}{4a}-c\right) \\
        &= (ax^2 + bx + c) + \left(\dfrac {b^2}{4a}-\dfrac{4ac}{4a}\right) \\
        &= (ax^2 + bx + c) + \left(\dfrac {b^2-4ac}{4a}\right) \\
        a \left( x + \dfrac {b}{2a} \right)^2 &= (ax^2 + bx + c) + \left(\dfrac {\Delta}{4a}\right)
    \end{align*}

    Ainsi, on peut passer le terme $\left(\dfrac {\Delta}{4a}\right)$ de l'autre côté de l'égalité. On trouve alors l'égalité :

    $a \left( x + \dfrac {b}{2a} \right)^2 - \left( \dfrac {\Delta}{4a} \right) = ax^2+bx+c = f(x)$  
    Conclusion: on a bien <enc>$f(x) = a \left(x+\dfrac {b}{2a}\right)^2 - \dfrac {\Delta}{4a}= ax^2+bx+c $</enc>

    :three: Notons ensuite <enc>$\alpha=-\dfrac {b}{2a}$</enc> et <enc>$\beta=-\dfrac {b^2-4ac}{4a}=- \dfrac{\Delta}{4a}$</enc> $\,$.
    
    D'après ce qui précède, on peut donc dire que :  

    * $x-\alpha=x-\left(-\dfrac {b}{2a}\right) = x+\dfrac {b}{2a}$
    * $f(\alpha)=a(\alpha - \alpha)^2+\beta = a\times0^2+\beta=0+\beta = \beta = -\dfrac {\Delta}{4a}$
    * $f(x)=a(x - \alpha)^2+\beta$ pour tout réel $x$

!!! ex
    Écrire sous forme canonique les trinômes suivants :

    |Grâce à une identité <br/>remarquable (laquelle ?) :<br/> "À la main"|Grâce à la formule<br/>$f(x)=a\left(x-\alpha\right)^2+\beta$<br/>avec $\alpha=-\dfrac {b}{2a}$, $\beta=\dfrac {-\Delta}{4a}$|
    |:-|:-|
    |:one: $3x^2 – 12x + 9=$<br/><hr/><hr/><hr/><hr/><hr/><hr/>|$3x^2 – 12x + 9=$<br/><hr/><hr/><hr/><hr/><hr/><hr/>|
    |:two: $2x^2 – 12x + 28=$<br/><hr/><hr/><hr/><hr/><hr/><hr/>|$2x^2 – 12x + 28=$<br/><hr/><hr/><hr/><hr/><hr/><hr/>|
    |:three: $5x^2 – 10x + 5=$<br/><hr/><hr/><hr/><hr/><hr/><hr/>|$5x^2 – 10x + 5=$<br/><hr/><hr/><hr/><hr/><hr/><hr/>|

## Factorisation de $ax^2 + bx + c$, avec $a\neq 0$ :

!!! thm "'Factorisation d'un trinôme' ou Écriture sous 'Forme Factorisée'"
    Pour factoriser un trinôme $ax^2 + bx + c$ (avec $a\neq 0$), il faut commencer par calculer
    le discriminant $\Delta=b^2-4ac$, puis, il ne peut exister que $3$ cas distincts :

    * :one:er Cas : Si $\Delta>0$, alors le trinôme **est factorisable**, sous la forme :  
    <center><enc>$ax^2 + bx + c = a(x - x_1)(x - x_2)$</enc></center>  
    Où l'on a posé : $\quad$ <enc>$x_1=\dfrac {-b-\sqrt \Delta}{2a}$</enc> et  $\quad$ <enc>$x_2=\dfrac {-b+\sqrt \Delta}{2a}$</enc> 
    * :two:ème Cas : Si $\Delta=0$, alors le trinôme **est factorisable**, sous la forme :  
    <center><enc>$ax^2 + bx + c = a(x - x_0)^2$</enc></center>
    Où l'on a posé : $\quad$ <enc>$x_0=\dfrac {-b}{2a}$</enc> ($=x_1=x_2$ lorsque $\Delta=0$)
    * :three:ème Cas : Si $\Delta<0$ :	alors le trinôme n'est **pas factorisable**.

!!! démo
    (En classe)

!!! ex "Factoriser les trinômes suivants lorsque c'est possible"
    1. $2x^2 – 3x + 1$
    1. $x^2 –4x + 5$
    1. $–3x^2 + 10x–7$
    1. $3x^2 –12x + 12$

!!! thm "(Courbe d'un trinôme du second degré, Extremum et Factorisation)"
    * La courbe d'une fonction trinôme s'appelle une <red>Parabole</red>.
    * Elle admet un **extremum absolu**, appelé <red>Sommet</red> S de la parabole, en $x_0 = \dfrac {-b}{2a}$:  <enc>$S\left(\dfrac {-b}{2a}; \dfrac {-\Delta}{4a}\right)$</enc>  
    Cet extremum est un **minimum** (lorsque $a>0$) ou un **maximum** (lorsque $a<0$).
    
    * L'orientation de sa branche et le nombre de points d'intersection avec l'axe des abscisses dépendent du signe de $a$ et du signe de $\Delta$ de la manière suivante :

    |>||>|>|Signe de $\Delta$|
    |:-:|:-:|:-:|:-:|:-:|
    |    >     |  | $\Delta>0$ | $\Delta=0$ | $\Delta<0$ |
    |Signe de a| $a>0$ | <iframe src="https://www.geogebra.org/classic/vzy3fymv?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe><br/>$ax^2+bx+c=a(x-x_1)(x-x_2)$|<iframe src="https://www.geogebra.org/classic/wmmhva5v?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe><br/>$ax^2+bx+c=a(x-x_0)^2$| <iframe src="https://www.geogebra.org/classic/yjdzz28v?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe><br/>$ax^2+bx+c$ non factorisable|
    |^|^|>|>|concavité tournée *vers les $y$ positifs*<br/> ou branche *vers le haut*|
    |^         | $a<0$ | <iframe src="https://www.geogebra.org/classic/yhetyvbs?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe><br/>$ax^2+bx+c=a(x-x_1)(x-x_2)$ | <iframe src="https://www.geogebra.org/classic/k9zxy3ke?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe><br/>$ax^2+bx+c=a(x-x_0)^2$|<iframe src="https://www.geogebra.org/classic/kbeepbae?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe><br/>$ax^2+bx+c$ non factorisable|
    |^|^|>|>|concavité tournée *vers les $y$ négatifs*<br/> ou branche *vers le bas*|
    |>|Nombre de Points d'intersection<br/> avec l'axe des $x$|$2$ points d'intersection,<br/>entre la parabole et l'axe des $x$,<br/>d'abscisses $x_1$ et $x_2$|$1$ seul point d'intersection,<br/>appelé un **point double**,<br/>entre la parabole et l'axe des $x$,<br/>d'abscisse $x_0$|Aucun point d'intersection|


!!! ex
    Soit $f(x) = –2x^2+5x-1$  
    1. Déterminer les coordonnées du sommet $S$ de la parabole représentant $f$  
    2. Déterminer les abscisses $x_1$ et $x_2$ des points d'intersection (s'ils existent) de la parabole $\mathcal P$ avec l'axe des $x$ 

## Équation $ax^2 + bx + c = 0$, avec $a\neq0$

### Exemples d'Équations particulières

#### Exemples d'Équations particulières dans lesquelles $b = 0$

1. $3x^2 – 5 = 0$
1. $2x^2 + 7 = 0$

Conclusion dans le cas où $b = 0$?

#### Exemple d'Équations particuliers dans lesquelles $c = 0$ : 

1.$2x^2 – 3x = 0$  
Conclusion dans le cas où $c = 0$?

### Cas général : Résolution de $ax^2 + bx + c = 0$ avec $a\neq0$

!!! thm "Résolution de l'équation $ax^2 + bx + c = 0$ avec $a\neq0$"
    Pour résoudre l'équation $ax^2 + bx + c = 0$ (avec $a\neq0$), il faut commencer par calculer
    le discriminant $\Delta=b^2-4ac$, puis :

    * :one:er Cas : Si $\Delta>0$, alors il y a deux *solutions* à cette équation, appelées les <red>racines</red> de l'équation, qui sont  $x_1$ et $x_2$ :  
    Où l'on a posé :  <enc>$x_1=\dfrac {-b-\sqrt \Delta}{2a}$</enc> $\quad $ et <enc>$\quad$ $x_2=\dfrac {-b+\sqrt \Delta}{2a}$</enc>
    * :two:ème Cas : Si $\Delta=0$, alors il y a une seule *solution (double)* à cette équation, appelé <red>racine double</red> qui est $x_0$ :  
    Où l'on a posé <enc>$x_0 = \dfrac {-b}{2a}$</enc> $(= x_1 = x_2)$  
    * :three:ème Cas : Si $\Delta< 0$ :	alors cette équation n'admet pas de "Solutions" / pas de "Racines".

!!! ex
    !!! col _2
        === "Exemple Résolu"
            Résoudre l'équation $3x^2-5x+1=0$  
            Commençons par calculer $\Delta$ :  

            \begin{align*}
            \Delta &= b^2-4ac \\
             &= (–5)^2 – 4\times3\times1 \\
             &= 25 – 12 \\
             &=  13>0
            \end{align*}
            
            Puisque $\Delta>0$,
            l'équation admet 2 solutions :
            !!! col _2
                \begin{align*}
                x_1 &= \dfrac {-b-\sqrt \Delta}{2a} \\
                  &=\dfrac {-(-5)-\sqrt {13}}{2\times3} \\
                  &=\dfrac {5-\sqrt {13}}{6}
                \end{align*}
            !!! col _2
                \begin{align*}
                x_2 &= \dfrac {-b+\sqrt \Delta}{2a} \\
                  &=\dfrac {-(-5)+\sqrt 13}{2\times3} \\
                  &=\dfrac {5+\sqrt 13}{6}
                \end{align*}
            $S = \left\{ \dfrac {5-\sqrt {13}}{6}; \dfrac {5+\sqrt {13}}{6} \right\}$
    !!! col _2
        === "Exercice 1"
            Résoudre l'équation $2x^2 + 3x – 5 = 0$
        === "Exercice 2"
            Résoudre l'équation $3x^2 - 18x + 27 = 0$


## Signe de $ax^2 + bx + c$, avec $a\neq0$ :

!!! thm
    Pour étudier le signe du trinôme $ax2 + bx + c$ (avec $a\neq0$), il faut commencer par calculer le discriminant $\Delta=b^2-4ac$, puis :

    * :one:er Cas : Si $\Delta>0$, alors :
        * le trinôme $ax^2 + bx + c$ est du signe de $a$ à l'extérieur des racines $x_1$ et $x_2$, càd sur $]-\infty; x_1[\cup]x_2 ;+\infty[$  
        De plus, $ax^2+bx+c=0$ lorsque $x=x_1$ ou $x=x_2$  
        Où l'on a posé : $\quad $ <enc>$x_1= \dfrac {-b-\sqrt \Delta}{2a}$</enc> $\quad$ et $\quad$ <enc>$x_2= \dfrac {-b+\sqrt \Delta}{2a}$</enc>
        * le trinôme $ax^2 + bx + c$ est du signe de $(–a)$ entre les racines $x_1$ et $x_2$, càd sur $[x_1 ;x_2]$
    * :two:ème Cas : Si $\Delta=0$, alors le trinôme $ax^2 + bx + c$ est toujours du signe de $a$.  
        De plus, $ax^2+bx+c = 0$ lorsque $x = x_0$  
        Où l'on a posé $x_0=\dfrac {-b}{2a}$ $\quad$ ($=x_1 = x_2$)
    * :three:ème Cas :Si $\Delta<0$, alors $ax^2 + bx + c$ est toujours du signe de $a$.

!!! démo
    (en classe)

!!! ex
    Étudier le signe des trinômes suivants sur $\mathbb R$ :

    1. $-x^2+2x+5$  
    1. $3x^2-x-1$

!!! ex
    Résoudre les inéquations suivantes sur $\mathbb R$ :

    1. $x^2 + 3x + 2 < 0$
    1. $-x^2 + 5x - 1 < 0$
    1. $\dfrac {x}{x+2} - \dfrac {1}{x} \leq \dfrac {-2}{3}$
        1. Déterminer l'ensemble de définition $\mathcal D$ de cette inéquation
        1. En déduire une résolution de l'inéquation