# <red>Corrigé</red> DM Équations et Inéquations avec Exponentielle

A Rendre pour le Jour de la Rentrée : <span style="font-size:1.5em;">**Lundi 26 Avril 2021**</span>

!!! ex
    Résoudre sur l'intervalle $\mathbb R$, chacune des équations demandées ci-dessous. En justifiant et en rédigeant.

    1. $e^{x} = -2$

        $S = \varnothing$ car l'exponentielle est strictement positive sur $\mathbb R$

    2. $e^{x} = 0$

        $S = \varnothing$ car l'exponentielle est strictement positive sur $\mathbb R$

    3. $e^{3x+1} = e^{5x+3}$

        $\Leftrightarrow 3x+1=5x+3$
        $\Leftrightarrow -2=2x$
        $\Leftrightarrow x=-1$
        $S = \{ -1\}$

    4. $e^{5x^2+x} = e^{4x+1}$

        $\Leftrightarrow 5x^2+x=4x+1$
        $\Leftrightarrow 5x^2-3x-1 = 0$
        $\Delta = b^2-4ac = (-3)^2-4\times5\times(-1)=9+20=29>0$
        $x_1 = \displaystyle \frac{-b-\sqrt \Delta}{2a} = \frac{+3-\sqrt {29}}{2\times5} = \frac{3-\sqrt {29}}{10} \approx -0,239$
        $x_2 = \displaystyle  \frac{-b+\sqrt \Delta}{2a} = \frac{+3+\sqrt {29}}{2\times5} = \frac{3+\sqrt {29}}{10} \approx 0,839$
        
        $\displaystyle  S = \{ \frac{3-\sqrt {29}}{10}; \frac{3+\sqrt {29}}{10} \} \approx \{ -0,239; 0,839 \}$

!!! ex
    Résoudre sur l'intervalle $\mathbb R$, chacune des inéquations demandées ci-dessous. En justifiant et en rédigeant.

    1. $e^{x} < -1$

        $S = \varnothing$ car l'exponentielle est strictement positive sur $\mathbb R$

    2. $e^{x} \ge 0$

        $S = \mathbb R$ car l'exponentielle est strictement positive sur $\mathbb R$, donc à plus forte raison, positive sur $\mathbb R$,

    3. $e^{7x+3} \le e^{4x+5}$

        $\Leftrightarrow 7x+3 \le 4x+5$ car l'exponentielle est strictement croissante sur $\mathbb R$
        $\Leftrightarrow 3x \le 2$
        $\displaystyle \Leftrightarrow x \le \frac 23$
        $\displaystyle S = \bigg] -\infty; \frac 23 \bigg]$

    4. $e^{x^2+2x} \ge e^{4x+3}$

        $\Leftrightarrow x^2 + 2x \ge 4x+3$ car l'exponentielle est strictement croissante sur $\mathbb R$
        $\Leftrightarrow x^2-2x-3 \ge 0$
        $\Delta = b^2-4ac = (-2)^2-4\times1\times(-3) = 4+12 = 16>0$
        $x_1 = \displaystyle \frac{-b-\sqrt \Delta}{2a} = \frac{+2-\sqrt {16}}{2\times1} = \frac{2-4}{2} = \frac{-2}{2} = -1$
        $x_2 = \displaystyle \frac{-b+\sqrt \Delta}{2a} = \frac{+2+\sqrt {16}}{2\times1} = \frac{2+4}{2} = \frac{6}{2} = 3$
        De plus, le cours nous dit que le trinône $x^2-2x-3$ est du signe de $-a=-1<0$, donc négatif, entre les racines, d'où le tableau des signes du trinôme:

        | $x$ | $-\infty$ | $x_1$ | $x_2$ | $+\infty$ |
        |:-:|:-:|:-:|:-:|:-:|:-:|
        | Signe de $x^2-2x-3$ | $+$ | $-$ | $+$ |

        Autrement le trinôme $x^2-2x-3$ est positif ($\ge 0$) sur $]-\infty; -1] \cup [3; +\infty[$

        $\displaystyle  S = ]-\infty; -1] \cup [3; +\infty[$
