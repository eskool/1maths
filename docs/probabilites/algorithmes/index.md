# 1MATHS : Algorithmes et Probabilités

## Algorithmes et Probabilités

|Algorithmes Trigonométrie|
|:-:|
|Méthode de Monte-Carlo : estimation de l’aire sous la parabole, estimation du nombre $\pi$.|
|Algorithme renvoyant l’espérance, la variance ou l‘écart type d’une variable aléatoire.|
|Fréquence d’apparition des lettres d’un texte donné, en français, en anglais.|

## Expérimentations

Le travail expérimental de simulation d’échantillons prolonge celui entrepris en seconde.  
L’objectif est de faire percevoir le principe de l’estimation de l’espérance d’une variable aléatoire, ou de la moyenne d’une variable statistique dans une population, par une moyenne observée sur un échantillon.

* Simuler une variable aléatoire avec Python.
* Lire, comprendre et écrire une fonction Python renvoyant la moyenne d’un échantillon de taille $n$ d’une variable aléatoire.
* Étudier sur des exemples la distance entre la moyenne d’un échantillon simulé de taille $n$ d’une variable aléatoire et l’espérance de cette variable aléatoire.
* Simuler, avec Python ou un tableur, $N$ échantillons de taille $n$ d’une variable aléatoire, d’espérance $\mu$ et d’écart type $\sigma$. Si $m$ désigne la moyenne d’un échantillon, calculer la proportion des cas où l’écart entre $m$ et $\mu$ est inférieur ou égal à $\dfrac {n}{\sigma^2}$


