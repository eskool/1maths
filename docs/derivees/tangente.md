# 1MATHS : Tangente à une Courbe en un point <span style="font-size:2em;">$A$</span>

## Définition

!!! def "Tangente à une Courbe"
    La tangente à la courbe $\mc C$ d'une fonction $f$ en un point $A \coord {a}{f(a)}\in \mc C$, est **la droite** $(T_A)$ qui est ***la meilleure*** approximation locale de la courbe, autour du point $A\in \mc C$.  
    Autrement dit, la tangente $(T_A)$ est la droite qui ***épouse le mieux*** la forme de la courbe $\mc C$ de $f$, localement autour du point $A\in \mc C$

!!! remarque
    <env>En théorie</env> Une tangente à la courbe $C$ de $f$ en un point $A(a;f(a))$ **n'existe pas obligatoirement**, certaines courbes pouvant être très complexes (en certains points, ou en plusieurs points). C'est parce que l'existence d'une tangente en $A(a;f(a))$ est liée à l'existence du nombre dérivé $f'(a)$, qui n'existe pas obligatoirement.
    <env>En pratique</env>, néanmoins, la tangente $(T_A)$ existe pour toutes les fonctions usuelles, en la plupart des points $A(a;f(a))$ de leurs courbes $\mc C$. Par la suite, nous supposerons toujours que la tangente en un point $A$ existe.

Comme toutes les droites, la tangente au point $A(a;f(a))$, admet une équation de droite.  
Une question naturelle qui vient à l'esprit est alors :

!!! Problématique
    Comment déterminer (graphiquement, ou par le calcul) l'équation de la tangente $(T_A)$, lorsque l'on connaît *seulement* la fonction $f$ et le point $A\coord {a}{f(a)}$

## Interprétation Graphique : Tangente et nombre dérivé $f'(a)$

On sait que le nombre dérivé $f'(a)$ est la limite du taux d'accroissement $T(a ;x)=\dfrac {f(x)−f(a)}{x-a}$ lorsque $x\to a$, ou bien, ce qui revient au même: 

!!! pte "$f'(a)$ et Limite des Taux de Variation"
    Lorsqu'un point mobile <green>$M\coord {x}{f(x)}\in \mc C$</green> s'approche (infiniment) du point <green>$A$</green>, tout en restant sur la courbe <green>$\mc C$</green>, alors la <green>droite $(AM)$</green> s'approche (infiniment) de la <red>la tangente $(T_A)$ à la courbe $\mc C$ en $A$</red>

On en déduit que :

!!! thm "Interprétation graphique du nombre dérivé $f'(a)$"
    Le nombre dérivé $f'(a)$ est le coefficient directeur/la pente de la tangente $(T_A)$ à la courbe $\mc C$ de $f$ en $A(a;f(a))$

<iframe id="tangente" src="https://www.geogebra.org/classic/jksxdckv?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

!!! pte "Lien entre Tangente et Existence de $f'(a)$"
    Dire que $f$ est dérivable en $a$
    $\ssi$ le nombre dérivé $f'(a)$ existe  
    $\ssi$ la tangente $(T_A)$ an $A(a;f(a))$ existe et elle admet un coefficient directeur $f'(a)$ fini     
    $\ssi$ la tangente $(T_A)$ an $A(a;f(a))$ existe et elle est une droite **oblique** ou **horizontale**     

!!! pte "Lien entre Tangente et NON existence de $f'(a)$"
    Dire que $f$ n'est pas dérivable en $a$ $\ssi$ le nombre dérivé $f'(a)$ n'existe pas $\ssi$ :

    * ou bien la tangente n'existe pas (car la limite en elle-même du taux de variation n'existe pas)
    * ou bien la tangente est verticale (car la limite est infinie : $+\infty$ ou $-÷infty$)

!!! ex "Détermination Graphique du nombre dérivé $f'(a)$"
    Sur la courbe dynamique $\mc C$ suivante, déplacer le point $A$ afin de déterminer/lire graphiquement les nombres dérivés suivants :

    1. $f'(–2)$
    1. $f'(-1)$
    1. $f'(0)$
    1. $f'(1)$
    1. $f'(2)$

    <iframe id="exo-deteremination-graphique-f" src="https://www.geogebra.org/classic/xcvp5uj4?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

## Équation de la Tangente à une Courbe

!!! thm
    Soit $f$ une fonction dérivable en $a$.  
    L'Équation de la tangente à la courbe $\mc C$ au point $A(a ;f(a))$ est :  
    <center><enc>$(T_A) : y = f'(a) (x – a) + f(a)$</enc></center>
    
<env>Truc mnémotechnique</env> cela revient à écrire $f'(a) = \dfrac {f(x)-f(a)}{x-a} \approx \dfrac {\Delta y}{\Delta x}$  
Ce qui revient à dire que le nombre dérivé $f'(a)$ est un taux de variation (c'est cohérent)

!!! ex "Détermination Graphique de l'équation de la tangente $T$"
    Sur la courbe dynamique $\mcC$ suivante, déplacer le point $A$ afin de déterminer graphiquement les équations de la tangente aux points $A(a;f(a))$ suivants :

    1. Équation de la tangente $(T_A)$ au point $a=0$
    1. Équation de la tangente $(T_A)$ au point $a=1$
    1. Équation de la tangente $(T_A)$ au point $a=2$
    1. Équation de la tangente $(T_A)$ au point $a=3$
    1. Équation de la tangente $(T_A)$ au point $a=4$
    1. Équation de la tangente $(T_A)$ au point $a=5$

    <iframe id="equation-tangente-graphiquement" src="https://www.geogebra.org/classic/ahekk5nz?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

!!! ex "Détermination par le Calcul de l'équation de la tangente"
    Soit $f$ définie par $f(x) = 5x^2$.  
    Considérons un nombre réel $a$ quelconque.  

    1. Déterminer $T(a ;a+h)$ en fonction de $h$
    1. En déduire l'expression du nombre dérivé $f'(a)$
    1. En déduire l'équation de la tangente $(T_A)$ à $\mc C$ en $A \coord {2}{f(2)}$ 
    1. En déduire l'équation de la tangente $(T_B)$ à $\mc C$ en $B \coord {-1}{f(-1)}$
