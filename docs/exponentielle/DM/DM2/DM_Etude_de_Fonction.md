# DM Étude de fonction avec une Exponentielle

Date de Rendu : <span style="font-size:1.5em; padding:5px;border:3px solid red; border-radius:8px;">**Lundi 3 mai 2021**</span>

Mode de Rendu :

* Groupe 1 (ayant cours en présentiel Lundi prochain) : **en main propre au Professeur** le Lundi 3 mai
* Groupe 2 (n'ayant PAS cours en présentiel Lundi prochian) : **dans le casier du Professeur** le Lundi 3 mai

!!! ex
    Soit $f$ la fonction définie sur $I=\mathbb R$, par $f(x)= (3x+2) \times e^{0.6x}$

    1. Calculer la fonction dérivée $f'(x)$. Puis, Factoriser $f'(x)$ du mieux que vous pouvez
    <env>**Aide**</env> Pour cela, on pourra remarquer que :

        * $f$ est de la forme $f=u\times v$, donc <enc>$f'=u'v+uv'$</enc>
        * <env>Rappel</env> la dérivée de $e^{kx}$ est $k \times e^{kx}$, où $k$ désigne une constante quelconque, donc :

    <center>

    <enc>$(e^{kx})' = k\times e^{kx}$</enc>

    </center>

    2. Étudier le signe de $f'(x)$, autrement dit : Résoudre l'inéquation $f'(x)>0$

    3. En déduire le Tableau de Variation de $f$ sur $\mathbb R$

    4. Étudier les extrema locaux sur $\mathbb R$:

        * Existe-t-il des extrema locaux? 
        * Si NON, pourquoi?
        * Si OUI:
        * Déterminer les extrema locaux
        * Est-ce que ce sont des minima? des maxima? pourquoi?
        * En quelles valeurs sont-ils atteints?

    5. Faites un graphique (approximatif) de la courbe de $f$ dans un repère. Unités: 

        * Une unité sur l'axe des $x$ correspond à $1$ cm.
        * Une unité sur l'axe des $y$ correspond à $1$ cm.
