# 1MATHS : Modes de Génération de Suites

Il existe plusieurs modes pour créer/définir une suite, oon parle de <red>générer une suite</red> :

## par une formule explicite $u_n=f(n)$

Ce mode de génération d'une suite est semblable à celui d'une fonction $f$, restreinte aux seules valeurs entières $x=n$.

!!! exp "$u_n = \dfrac {1}{2}n +1$"
    On peut définir une suite $(u_n)$ par une <red>formule explicite</RED>, par exemple, $u_n=f(n)=\dfrac {1}{2}n +1$  
    Chaque terme $u_n$ de la suite $(u_n)$ est alors calculable de la manière suivante :  

    * $u_0=\dfrac 12\times 0 + 1=0 + 1=1$
    * $u_1=\dfrac 12\times 1 + 1=\dfrac 12 + 1=\dfrac 32$
    * $u_2=\dfrac 12\times 2 + 1=\dfrac 22 + 1=2$
    * $u_3=\dfrac 12\times 3 + 1=\dfrac 32 + 1=\dfrac 52$
    * etc..

## par une formule de récurrence $u_{n+1}=f(u_n,n)$

Ce mode de génération se caractérise par le calcul du terme suivant, lorsque l'on connaît déjà le terme courant. Ce mode de génération présuppose (contrairement au mode de génération fonctionnel précédent), que l'on connaisse le premier terme.

!!! exp "$u_{n+1}=\dfrac 12 u_n+1$ et $u_0=1$"
    On peut définir une suite $(u_n)$ par une <red>formule de récurrence</red> de type $u_{n+1}=f(u_n)$, par exemple $u_{n+1}=\dfrac 12 u_n+1$ et $u_0=1$  
    Chaque terme $u_n$ de la suite $(u_n)$ est alors calculable de la manière suivante :  

    * $u_0=1$ : déjà donné, obligatoirement
    * $u_1=\dfrac 12 \times u_0+1=\dfrac 12 \times 1 + 1 = \dfrac 32$
    * $u_2=\dfrac 12 \times u_1+1=\dfrac 12 \times \dfrac 32 + 1 = \dfrac 34 + 1=\dfrac 74$
    * $u_3=\dfrac 12 \times u_2+1=\dfrac 12 \times \dfrac 74 + 1 = \dfrac 78 + 1=\dfrac {15}{8}$
    * etc..

!!! exp "$u_{n+1}=2 u_n+3n-1$ et $u_0=1$"
    On peut définir une suite $(u_n)$ par une <red>formule de récurrence</red> de type $u_{n+1}=f(u_n, n)$, par exemple $u_{n+1}=2 u_n + 3n - 1$ et $u_0=1$  
    Chaque terme $u_n$ de la suite $(u_n)$ est alors calculable de la manière suivante :  

    * Pour $n=0$, $u_0 = 1$ : déjà donné, obligatoirement
    * Pour $n=1$, $u_1 = 2\times \redbox{u_0} + 3\times \redbox{0} - 1 = 2\times 1 + 3\times 0 - 1 = 1$
    * Pour $n=2$, $u_2 = 2\times \redbox{u_1} + 3\times \redbox{1} - 1 = 2\times 1 + 3\times 1 - 1 = 4$
    * Pour $n=3$, $u_3 = 2\times \redbox{u_2} + 3\times \redbox{2} - 1 = 2\times 4 + 3\times 2 - 1 = 13$
    * etc..
