# Cours 1ère : La Fonction Exponentielle

## Théorème d'Existence

### Non Annulation d'une fonction particulière

!!! pte
    Si $f$ est une fonction dérivable sur $\mathbb R$ telle que $f'=f$ et $f(0)=1$ alors $f$ ne s'annule pas sur $\mathbb R$

??? preuve
    Notons $\varphi$ la fonction définie sur $\mathbb R$, par $\varphi(x)=f(x)\times f(-x)$
    La fonction $\varphi$ est dérivable sur $\mathbb R$ et pour tout $x$:
    $\varphi'(x)=f'(x)f(-x)+f(x)\times \left[ -f'(-x) \right]$
    $\varphi'(x)=f(x)f(-x) - f(x)\times f(-x)$
    $\varphi'(x)=0$
    donc $\varphi$ est une fonction constante  sur $\mathbb R$.
    De plus $\varphi(0)=f(0)\times f(-0)=f(0)^2=1^2=1$
    donc pour tout $x \in \mathbb R$, $\varphi(x)=1$
    donc pour tout $x \in \mathbb R$, $f(x)\times f(-x)=1$
    En particulier, on en déduit que pour tout $x \in \mathbb R$, $f(x) \ne 0$

### Existence et Unicité de la Fonction Exponentielle

!!! def
    Il **existe** une **unique** fonction dérivable $f(x)$, définie sur l'ensemble $\mathbb{R}$, telle que $f'=f$ et $f(0)=1$
    Cette fonction est notée $exp(x)$, et s'appelle (LA) **fonction exponentielle**.

**Conséquences immédiates :**
La démonstration précédente admet deux Conséquences importantes:

!!! pte "Conséquence N°1"
    La fonction exponentielle $x\mapsto exp(x) $ ne s'annule pas sur $\mathbb R$:
    $\forall x \in \mathbb R, \quad exp(x)\ne 0$

!!! pte "Conséquence N°2"
    $\forall x \in \mathbb R, \quad exp(x)\times exp(-x)=1$, ou ce qui est équivalent:
    $\forall x \in \mathbb R, \quad exp(-x)=\dfrac {1}{exp(x)}$

## Dérivabilité de la fonction Exponentielle

### Dérivée de la fonction Exponentielle $x\mapsto exp(x)$

!!! pte
    La fonction exponentielle est définie et dérivable sur $\mathbb R$, et sa dérivée est égale à elle-même:
    $\forall x \in \mathbb R, \quad exp'(x)=exp(x)$

### Dérivée de la fonction $x \mapsto exp(ax+b)$

!!! pte
    La fonction $g:x \mapsto exp(ax+b)$ est définie et dérivable sur $\mathbb R$, et sa dérivée est égale à:
    $\forall x \in \mathbb R, \quad g'(x)=\left( exp(ax+b) \right)' =a\times exp(ax+b)$

### Dérivée de la fonction $x \mapsto exp(u(x))$ (HORS-PROGRAMME)

!!! pte
    Soit $u:x\mapsto u(x)$ une fonction définie et dérivable sur $\mathbb R$
    La fonction $g:x \mapsto exp(u(x))$ est définie et dérivable sur $\mathbb R$, et sa dérivée est égale à:
    $\forall x \in \mathbb R, \quad g'(x)=\left( exp(u(x)) \right)' =u'(x)\times exp(u(x))$

## Premières Propriétés de $exp()$

### La Propriété Fondamentale de l'Exponentielle

!!! pte "La fonction exponentielle transforme une somme en produit"
    $exp(x+y)=exp(x)\times exp(y) \quad \forall x,y \in \mathbb R$

    Cette propriété se généralise avec plusieurs termes:

    $exp(x_1+x_2+...+x_p)=exp(x_1)\times exp(x_2) \times ... \times \exp(x_p) \quad \forall x_1,x_2,...,x_p \in \mathbb R$

??? preuve
    * $y$ étant un nombre réel donné (constant), on note $f$ la fonction définie sur $\mathbb R$ par $f(x)=\dfrac {exp(x+y)} {exp(x)}$. $f$ est le quotient d'un de deux fonctions dérivables sur $\mathbb R$, donc est dérivable sur $\mathbb R$, et l'on a:
    $f'(x)=\dfrac {exp(x+y)\times exp(x)-exp(x+y)\times exp(x)}{[exp(x)]^2}=0$
    donc $f$ est une fonction constante sur $\mathbb R$
    De plus, $f(0)=\dfrac {exp(y)}{exp(0)}=\dfrac {exp(y)} 1=exp(y)$
    donc pour tout $x \in \mathbb R$, $f(x)=exp(y)$
    donc pour tout $x \in \mathbb R$, $\dfrac {exp(x+y)}{exp(x)}=exp(y)$
    donc pour tout $x \in \mathbb R$, $exp(x+y)=exp(x)\times exp(y)$
    * pour $exp(x_1+x_2+...+x_p)$, il suffit de réutiliser la formule précédente plusieurs fois.

### Premières propriétés de l'Exponentielle

!!! pte "La fonction exponentielle transforme l'oppposé en l'inverse"
    Pour tout $x \in \mathbb R, \, exp(-x)=\dfrac {1}{exp(x)}$

??? preuve
    <env>Méthode 1</env> C'est une conséquence de la toute première propriété du cours (non annulation d'un2e fonction particulière)
    ou bien,  
    <env>Méthode 2</env> Pour tout réel $x$, Calculons de deux manières différentes la valeur $exp(x-x)$:

    * D'une part, $exp(x-x)=exp(0)=1$
    * D'autre part, $exp(x-x)=exp(x+(-x))=exp(x)\times exp(-x)$
    donc $\forall x \in \mathbb R, \quad exp(x)\times exp(-x)=1$
    Conclusion: $\forall x \in \mathbb R, \quad exp(-x)=\dfrac {1}{exp(x)}$ car on sait que l'exponentielle ne s'annule jamais

!!! pte "La fonction exponentielle transforme une soustraction en quotient"
    Pour tous $x,y \in \mathbb R, \, exp(x-y)=\dfrac {exp(x)}{exp(y)}$

??? preuve
    Pour tous réels $x$ et $y$, 
    $exp(x-y)=exp(x)\times exp(-y)=exp(x) \times \dfrac {1}{exp(y)}= \dfrac {exp(x)}{exp(y)}$

!!! pte "La fonction exponentielle transforme un produit en puissance"
    Pour tout $n$ entier $\in \mathbb Z$ (positif ou négatif),
    $exp(nx)={exp(x)}^n$

??? preuve
    * Pour $n$ entier positif, $exp(nx)=exp(x+x+..+x)=exp(x) \times exp(x) \times ... \times exp(x) \quad (n fois)$
    Pour tout entier $n$, $exp(nx)=\left(exp(x) \right) ^n$
    * Pour $n$ entier négatif, notons $p$ l'entier opposé, donc $n=-p$ avec $p$ entier $\ge0$
    Remarquons tout d'abord que, pour tout $x \in \mathbb R$:
    $exp(nx)\times exp(-nx)=exp(nx-nx)=exp(0)=1$
    donc, $exp(nx)=\dfrac {1}{exp(-nx)}$ or $-n=-(-p)=+p\ge 0$
    donc, $exp(nx)=\dfrac {1}{exp(px)}=\dfrac {1}{(exp(x))^p}$ d'après ce qui précède pour $p$ entier $\ge 0$
    donc, $exp(nx)={exp(x)}^{-p}=exp(x)^n$ parce que $-p=n$

!!! pte "Conséquences: Nouvelle Notation $e^x$"
    * Pour tout entier $n \in \mathbb Z$, $\quad exp(n)={exp(1)}^n$ car pour $x=1$, $exp(1\times n)= exp(1)^n$
    * D'où l'idée de noter $e$ le nombre $e=exp(1)$ qui joue un rôle particulier
    * La calculatrice donne $e \approx 2,718281828...$
    * donc pour tout entier $n \in \mathbb Z$, $exp(n)=e^n$
    * :boom: **C'est pourquoi, on a l'idée de généraliser cette notation pour tout $x \in \mathbb R $**: :boom:  
    <center><enc>$exp(x)=e^x$</enc></center>

## Nouvelle Notation $x \mapsto e^x$

### Formulaire avec la Notation $x\mapsto e^x$

Les formules suivantes résument ce que nous connaissons déjà depuis le début du cours, ainsi que quelques nouvelles formules, mais avec la :boom: **nouvelle notation** $x \mapsto e^x$ :boom:

!!! thm "Formulaire sur l'Exponentielle :heart:"
    * L'Exponentielle ne s'annule jamais:  
    $\forall x \in \mathbb R$, <center>$e^x \ne 0$</center>
    * $e^0=1$ $\quad$ car $exp(0)=1$ par définition
    * **Valeur du nombre $e$** : on calcule $e^1 = e \approx 2,718281828...$
    * La fonction exponentielle transforme une somme en produit:  
    $\forall x,y \in \mathbb R$ <center>$e^{x+y}=e^x\times e^y$</center>  
    Cette propriété se généralise avec plusieurs termes:  
    $\forall x_1,x_2,...,x_p \in \mathbb R$,
    <center>$e^{x_1+x_2+...+x_p}=e^{x_1} \times e^{x_2} \times ... \times e^{x_p}$</center>
    * La fonction exponentielle transforme un produit en puissance:  
    $\forall n \in \mathbb Z$ (entier positif ou négatif),
    <center>$e^{nx}={\left(e^x\right)}^n$</center>
    * L'Exponentielle transforme une soustraction en quotient:  
    $\forall x,y \in \mathbb R$, <center>$e^{x-y}=\dfrac {e^x}{e^y}$</center>
    * L'Exponentielle transforme l'opposé en l'inverse:  
    $\forall x \in \mathbb R$, <center>$e^{-x}=\dfrac 1 {e^x}$</center>

### Dérivées avec la Notation $x\mapsto e^x$

#### Dérivée de la fonction $x \mapsto e^{x}$

!!! pte "Dérivée de la fonction Exponentielle $x \mapsto e^x$"
    La fonction exponentielle est définie et dérivable sur $\mathbb R$, et sa dérivée est égale à elle-même:  
    <center>$\forall x \in \mathbb R, \quad (e^x)'=e^x$</center>

#### Dérivée de la fonction $x \mapsto e^{ax+b}$

!!! pte "Dérivée de la fonction $x \mapsto e^{ax+b}$"
    La fonction $g:x \mapsto e^{ax+b}$ est définie et dérivable sur $\mathbb R$, et sa dérivée est égale à:  
    <center>$\forall x \in \mathbb R, \quad g'(x)=\left( e^{ax+b} \right)' =a\times e^{ax+b}$<center>

#### Dérivée de la fonction $x \mapsto e^{u(x)}$

!!! pte "Dérivée de la fonction $x \mapsto e^{u(x)}$"
    Soit $u:x\mapsto u(x)$ une fonction définie et dérivable sur $\mathbb R$
    La fonction $g:x \mapsto e^{u(x)}$ est définie et dérivable sur $\mathbb R$, et sa dérivée est égale à:  
    <center>$\forall x \in \mathbb R, \quad g'(x)=\left( e^{u(x)} \right)' =u'(x)\times e^{u(x)}$</center>

## Étude de la fonction Exponentielle

### Signe de l'Exponentielle

!!! pte
    La fonction Exponentielle est strictement positive sur $\mathbb R$
    Autrement dit: pour tout $x \in  \mathbb R$, $\quad e^x>0$
    (En particulier, la fonction exponentielle ne s'annule jamais, ce que nous savions déjà)

??? preuve
    D'une part : On sait que $e^{x}=e^{2\times \dfrac x 2}={ \left( e^{\dfrac x 2} \right) }^2\ge0 \quad$ pour tout $x \in \mathbb R$
    D'autre part, l'exponentielle ne s'annule jamais sur $\mathbb R$: pour tout $x \in \mathbb R$, $e^x \ne 0$

### Sens de variation de l'Exponentielle & Tableau de Variation

!!! pte
    La fonction exponentielle est strictement croissante sur $\mathbb R$

??? preuve
    La fonction exponentielle est définie et dérivable sur $\mathbb R$, et $(e^x)'=e^x>0$
    Sa dérivée étant strictement positive sur $\mathbb R$, la fonction exponentielle est strictement croissante sur $\mathbb R$

<center>

<iframe src="https://www.geogebra.org/classic/tf3txqdc?embed" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>
<figcaption><b>Fonction Exponentielle (de base e)</b>
</figcaption>

</center>

D'où le tableau de variation la fonction exponentielle sur $\m R$ :

```vartable
\begin{tikzpicture}
	\tkzTabInit[lgt=3, espcl=6]
		{$x$/1 , Signe de $f'(x) =e^x$/1.5 , Variations de $f(x)=e^x$/2.5}
		{$-\infty$, $+\infty$}
	\tkzTabLine{,+,}
	\tkzTabVar{- / $0$,  + / $+\infty$ }
	\tkzTabVal[draw]{1}{2}{0.35}{$0$}{$1$}
	\tkzTabVal[draw]{1}{2}{0.7}{$1$}{$e$}
\end{tikzpicture}
```

### Résolution d'Équations et Inéquations

!!! pte
    La fonction exponentielle est strictement croissante sur $\mathbb{R}$, donc :

    * $\forall x,y \in \mathbb R, \quad e^x=e^y \Leftrightarrow x=y$
    * $\forall x,y \in \mathbb R, \quad e^x<e^y \Leftrightarrow x<y$
    * $\forall x,y \in \mathbb R, \quad e^x \le e^y \Leftrightarrow x \le y$
    * $\forall x,y \in \mathbb R, \quad e^x>e^y \Leftrightarrow x>y$
    * $\forall x,y \in \mathbb R, \quad e^x \ge e^y \Leftrightarrow x \ge y$

!!! ex
    Résoudre dans $\mathbb R$ les équations et inéquations suivantes:  
    1. $e^x=1$  
    2. $e^{2x+1}=1$  
    3. $e^{3x+2}=e^{5x-1}$  
    4. $e^{x^2+6x}=e^{2x-1}$  
    5. $e^{x+3} \lt e^{2x+1}$  
    6. $e^{4x+1}\le e^{x+4}$  
    7. $e^{5x-2}> e^{x+2}$  
    8. $e^{8x-1}\ge e^{2x+3}$

## Les fonctions $t\mapsto e^{-kt}$ et $t\mapsto e^{kt}$ avec $k>0$

Dans toute la suite, on se donne un réel $k>0$ fixé.

### fonctions $t\mapsto e^{-kt}$

La fonction $f_k=t\mapsto e^{-kt}$ est définie et dérivable sur $\mathbb R$.
Pour tout réel $t$, $f'_k(t)=-ke^{-kt}<0$ sur $\mathbb R$, car :

* $-k<0$, et 
* $e^{-kt}>0$ pour tout réel $t \in \mathbb R$

Ceci prouve que la fonction $f_k$ est donc strictement décroissante sur $\mathbb R$
D'où le tableau de variation:

```vartable
  \begin{tikzpicture}
   \tkzTabInit[lgt=3, espcl=6]
   {$x$ /1, Signe de $f_k'(t) = -k e^{-kt}$ /1, Variations de $f_k(t) = k e^{kt}$ /1.5} {$-\infty$ , $+\infty$}
   \tkzTabLine{, -,} 
   \tkzTabVar{+/ $+\infty$, -/ $0$}
   \tkzTabVal{1}{2}{0.5}{$0$}{1} % On place 1, son antécédent est 0.
  \end{tikzpicture}
```

### fonctions $t\mapsto e^{kt}$

La fonction $f_k=t\mapsto e^{kt}$ est définie et dérivable sur $\mathbb R$.
Pour tout réel $t$, $f'_k(t)=ke^{kt}>0$ sur $\mathbb R$, car :

* $k>0$, et 
* $e^{kt}>0$ pour tout réel $t \in \mathbb R$

Ceci prouve que la fonction $f_k$ est donc strictement croissante sur $\mathbb R$
D'où le tableau de variation:

```vartable
\begin{tikzpicture}
\tkzTabInit[lgt=3, espcl=6]
{$x$ /1, Signe de $f_k'(t)=k e^{kt}$ /1, Variations de $f_k(t)= e^{kt}$ /1.5} {$-\infty$ , $+\infty$}
\tkzTabLine{, +,} 
\tkzTabVar{-/ $0$, +/ $+\infty$}
\tkzTabVal{1}{2}{0.5}{$0$}{1} % On place 1, son antécédent est 0.
\end{tikzpicture}
```
