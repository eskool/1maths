# 1MATHS : Suites Géométriques

## Définition

!!! def
    Une suite $(u_n)$ est dite <red>géométrique de raison $q$</red>, avec $q \in \mathbb R$, lorsque:  
    <center>Pour tout entier $n$, $\quad u_{n+1}=q\times u_n$</center>

!!! exp
    Pour tout entier $n$, $\quad u_{n+1}=3\times u_n$ avec $u_0=4$

    * La suite $(u_n)$ est une suite géométrique de raison $q=3$, et de terme initial $u_0=4$
    * Calculer les premiers termes: $u_1$, $u_2$, $u_3$, $u_4$  
    On sait que $u_1=3\times u_0=3\times 4= 12$  
    On sait que $u_2=3\times u_1=3\times 12= 36$  
    On sait que $u_3=3\times u_2=3\times 36= 108$  
    On sait que $u_4=3\times u_3=3\times 108= 324$

## Comment montrer qu'une suite est géométrique, ou pas

!!! pte
    Une suite $(u_n)$ est géométrique, de raison $q$  
    $\Leftrightarrow$ la suite de tous les quotients  $\dfrac {u_{n+1}}{u_n}$ est constante, et vaut (toujours) $q$

!!! ex "Montrer qu'une suite n'est PAS géométrique"
    Soit $(u_n)$ la suite définie par $u_n=n^2$  
    Montrer que la suite $(u_n)$ n'est pas géométrique

    ??? corr
        Calculons:

        * $u_0=0^2=0$
        * $u_1=1^2=1$
        * $u_2=2^2=4$
        * $u_3=3^2=9$

        Calculons les quotients suivants:

        * $\dfrac {u_2}{u_1}=\dfrac {4}{1}=4$
        * $\dfrac {u_3}{u_2}=\dfrac {9}{4}=2,25$

        Conclusion : $\dfrac {u_2}{u_1} \ne \dfrac {u_3}{u_2}$  
        Ceci prouve que PAS tous les quotients $\dfrac {u_{n+1}}{u_n}$ ne sont égaux entre eux.  
        donc que la suite $(u_n)$ n'est PAS géométrique

!!! ex "Montrer qu'une suite EST géométrique"
    Soit $(u_n)$ la suite définie par $u_n=3\times 4^n$ pour tout entier $n$  
    Montrer que la suite $(u_n)$ est géométrique.  
    On déterminera la raison $q$, et le terme initial $u_0$

    ??? corr
        Exprimons $u_{n+1}$ en fonction de $n$:  
        $u_{n+1}=3\times 4^{n+1}$  
        Calculons le quotient suivant en fonction de $n$:  
        Pour tout entier $n$, $\dfrac {u_{n+1}}{u_n} = \dfrac {3\times 4^{n+1}}{3\times 4^n}=\dfrac {3\times 4^n \times 4^1}{3\times 4^n}=4$  
        Ceci prouve que:  

        * la suite $(u_n)$ est géométrique
        * sa raison vaut $q=4$
        * le terme initial vaut $u_0=3\times 4^0=3\times 1=3$

## Exprimer $u_n$ en fonction de $n$

!!! pte
    Soit $(u_n)$ une suite géométrique, de raison $q$.
    Pour exprimer $u_n$ en fonction de $n$, on dispose des formules suivantes:

    * $u_n=u_0 \times q^n$
    * $u_n=u_1 \times q^{n-1}$
    * $u_n=u_2 \times q^{n-2}$
    * etc...
    * $u_n=u_p \times q^{n-p}$ où $p$ est un entier positif

!!! ex
    1. Soit $(u_n)$ une suite géométrique de raison $q=2$, et de terme initial $u_0=3$. Exprimer $u_n$ en fonction de $n$

        ??? corr
            On sait que $u_n=u_0\times q^{n}=3\times 2^n$

    2. Soit $(u_n)$ une suite géométrique de raison $q=4$, et de terme initial $u_1=5$. Exprimer $u_n$ en fonction de $n$

        ??? corr
            On sait que $u_n=u_1\times q^{n-1}=5\times 4^{n-1}$

## Somme des termes consécutifs d'une suite géométrique

!!! pte
    Pour tout entier $n$, et pour $q \ne 1$,  
    <center><enc>$
    \begin{align*}
    1+q+q^2+...+q^n &= \dfrac {1-q^{n+1}}{1-q} \\
        &= \dfrac {q^{n+1}-1}{q-1}
        \end{align*}
        $</enc></center>

!!! ex
    Calculer les sommes suivantes:

    1. $S=1+2+4+..+2^{10}$

        ??? corr
            $\begin{align*}
            S &= 1+2+4+..+2^{10} \\
                &= 1+2+2^2+..+2^{10} \\
                &= \dfrac {2^{10+1}-1}{2-1} \\
                &= \dfrac {2^{10+1}-1}{1} \\
                &= 2^{11}-1 \\
                &= 2047
            \end{align*}
            $

    2. $S=1+3+9+..+3^{24}$

        ??? corr
            $\begin{align*}
            S = 1+3+3^2+..+3^{24} &= \dfrac {3^{24+1}-1}{3-1} \\
                &=\dfrac {3^{25}-1}{2}
            \end{align*}$
    
    3. $S=1+\dfrac {1}{2}+\dfrac {1}{4}+..+\dfrac {1}{2^{30}}$
    
        ??? corr
            $\begin{align*}
            S = 1+\dfrac {1}{2}+\dfrac {1}{2^2}+..+\dfrac {1}{2^{30}} &= 1+\dfrac {1}{2}+\left( \dfrac {1}{2} \right)^2+..+\left( \dfrac {1}{2} \right)^{30}
            \end{align*}$
            donc $q=\dfrac 1 2$ et $n=30$
            
            $S = \dfrac {1 - \left( \dfrac 1 2 \right) ^{30+1}}{1 - \dfrac 1 2}$  
            $S = \dfrac {1- \left( \dfrac 1 2 \right) ^{31}}{\dfrac 1 2}$  
            $S = 2\times \left( 1- \left( \dfrac 1 2 \right) ^{31} \right)$  
            $S = 2\times \left( 1- \dfrac 1 {2^{31}} \right)$

    4. Exprimer $S$ en fonction de $n$, avec $S=1+2+4+...+2^{n}$
    
        ??? corr
            $S=1+2+4+..+2^{n}=1+2+2^2+..+2^{n}$
            $=\dfrac {2^{n+1}-1}{2-1}=\dfrac {2^{n+1}-1}{1}=2^{n+1}-1$

    5. Exprimer $S$ en fonction de $n$:
    $S=1+3+9+..+3^{n}$

    6. Exprimer $S$ en fonction de $n$:
    $S=1+\dfrac {1}{2}+\dfrac {1}{4}+..+\dfrac {1}{2^{n}}$

    7. Exprimer $S$ en fonction de $n$:
    $S=1+\dfrac {2}{3}+\dfrac {4}{9}+..+\dfrac {2^n}{3^{n}}$

    8. Exprimer $S$ en fonction de $n$:
    $S=2^{10}+2^{11}+..+2^n$

    9. Exprimer $S$ en fonction de $n$:
    $S=\dfrac {2^4}{3^4}+\dfrac {2^5}{3^5}+..+\dfrac {2^n}{3^{n}}$

!!! pte "Somme commençant par $u_0$"
    Soit $(u_n)$ une suite géométrique de raison $q$ et de terme initial $u_0$:  
    $\begin{align*}
    S = u_0+u_1+u_2+...+u_n &= u_0 \times \dfrac {1-q^{n+1}}{1-q} \\  
        &= u_{premier} \times \dfrac {1-q^{\text{NB de Termes}}}{1-q} \\
        &= u_{premier} \times \dfrac {q^{\text{NB de Termes}} -1}{q-1}
    \end{align*}$

!!! pte "Somme commençant par $u_a$"
    Soit $(u_n)$ une suite géométrique de raison $q$ et de terme initial $u_0$:  
    $\begin{align*}
    S = u_a+...+u_b &= u_a \times \dfrac {1-q^{b-a+1}}{1-q} \\
        &= u_{premier} \times \dfrac {1-q^{\text{NB de Termes}}}{1-q} \\
        &= u_{premier} \times \dfrac {q^{\text{NB de Termes}} -1}{q-1}
        \end{align*}$

## Limite des suites $(q^n)$

!!! pte
    Soit $q$ un réel.

    :one: Si $-1<q<1$, Alors $\displaystyle \lim_{n \to +\infty} q^n=0$  
    :two: Si $q>1$, Alors $\displaystyle \lim_{n \to +\infty} q^n=+\infty$  
    :three: Si $q=1$ Alors pour tout entier $n$, $q^n=1^n=1$ $\quad$ donc $\displaystyle \lim_{n \to +\infty} q^n=1$  
    :four: Si $q\le -1$, Alors **la suite $(q^n)$ n'admet pas de limite.**  
    En effet, la suite $(u_n)$ prend des valeurs alternées (resp. positives puis négatives) s'approchant respectivement et alternativement vers $+\infty$, et $-\infty$: Dans ce cas, on dit qu'*il n'existe pas de limite*.

!!! ex
    Déterminer la limite des suites $(u_n)$ suivantes

    1. $u_n=(0,2)^n$<stylo><rep>

        ??? corr
            $\displaystyle \lim_{n \to +\infty} (0,2)^n=0$ car $-1<q=0,2<1$

    2. $u_n=(0,98)^n$

        ??? corr
            $\displaystyle \lim_{n \to +\infty} (0,98)^n=0$ car $-1<q=0,98<1$

    3. $u_n=(1,04)^n$<stylo><rep>

        ??? corr
            $\displaystyle \lim_{n \to +\infty} (1,04)^n=+\infty$ car $q=1,04>1$

    4. $u_n=(13,58)^n$<stylo><rep>

        ??? corr
            $\displaystyle \lim_{n \to +\infty} (13,58)^n=+\infty$ car $q=13,58>1$

    5. $u_n=2\times (0,45)^n$
    
        ??? corr
            $\displaystyle \lim_{n \to +\infty} 2\times (0,45)^n=2\times 0$ car:
            $\displaystyle \lim_{n \to +\infty} (0,45)^n=0$
            d'après le théorème sur le produit des limites.

    6. $u_n=0,1\times 2^n$
    
        ??? corr
            $\displaystyle \lim_{n \to +\infty} 0,1\times 2^n=+\infty$ car
            $\displaystyle \lim_{n \to +\infty} 2^n=+\infty$  
            et d'après le théorème sur le produit de limites.

    7. $u_n=-3\times 5^n$
    
        ??? corr
            $\displaystyle \lim_{n \to +\infty} -3\times 5^n=-\infty$ car 
            $\displaystyle \lim_{n \to +\infty} 5^n=+\infty$
            et d'après le théorème sur le produit de limites.

    8. $u_n=4,5\times (0,3)^n$

## Limite de la Somme des termes consécutifs d'une suite Géométrique

!!! pte "Commençant par $u_0$"
    Soit $(u_n)$ une suite géométrique de raison $-1<q<1$  
    Soit $S_n=u_0+u_1+...+u_n$  
    Alors $\displaystyle \lim_{n \to +\infty} S_n=\dfrac {u_0} {1-q}=u_0\times \dfrac {1}{1-q}$

??? preuve
    Soit $S_n=u_0+u_1+u_2+...+u_n=u_0+u_0\times q^1+u_0\times q^2+...+u_0\times q^n$  
    donc $S_n=u_0\times \left( 1+q^1+q^2+...+ q^n\right)= u_0\times \dfrac{1-q^{n+1}}{1-q}$  
    $S_n= \dfrac{u_0}{1-q}\times (1-q^{n+1})$  
    Or $0<q<1$ donc $\displaystyle \lim_{n \to +\infty} q^{n+1}=0$ donc   $\displaystyle \lim_{n \to +\infty} (1-q^{n+1})=1-0=1$ d'après le théorème sur la soustraction de limites ("TSL")  
    Donc $\displaystyle \lim_{n \to +\infty} \dfrac{u_0}{1-q}\times (1-q^{n+1})=\dfrac{u_0}{1-q}\times 1=\dfrac{u_0}{1-q}$  
    d'après le théorème sur le produit de limites ("TPL")  
    donc $\displaystyle \lim_{n \to +\infty} u_n=\frac{u_0}{1-q}$

!!! ex
    Pour chacune des suites **géométriques** suivantes, calculer la limites de la somme $S_n$ indiquée:

    1. $(u_n)$ géom, de raison $q=\dfrac 12 $, $u_0=3$, $S_n=u_0+u_1+...+u_n$
    2. $(u_n)$ géom, de raison $q=\dfrac 23 $, $u_0=4$, $S_n=u_0+u_1+...+u_n$
    3. $(u_n)$ géom, de raison $q=0,48 $, $u_0=6$, $S_n=u_0+u_1+...+u_n$
